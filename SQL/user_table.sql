CREATE TABLE `tm_user` (
	`id` VARCHAR(255) NOT NULL COLLATE 'utf8_unicode_ci',
	`login` VARCHAR(255) NULL DEFAULT NULL COLLATE 'utf8_unicode_ci',
	`passwordHash` VARCHAR(255) NULL DEFAULT NULL COLLATE 'utf8_unicode_ci',
	`email` VARCHAR(255) NULL DEFAULT NULL COLLATE 'utf8_unicode_ci',
	`firstname` VARCHAR(255) NULL DEFAULT NULL COLLATE 'utf8_unicode_ci',
	`lastname` VARCHAR(255) NULL DEFAULT NULL COLLATE 'utf8_unicode_ci',
	`middlename` VARCHAR(255) NULL DEFAULT NULL COLLATE 'utf8_unicode_ci',
	`role` VARCHAR(50) NULL DEFAULT NULL COLLATE 'utf8_unicode_ci',
	`locked` BIT(1) NULL DEFAULT NULL,
	PRIMARY KEY (`id`) USING BTREE
)
COLLATE='utf8_unicode_ci'
ENGINE=InnoDB
;
