package ru.goloshchapov.tm;


import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import ru.goloshchapov.tm.endpoint.*;


public class ProjectEndpointTest {

    private static final SessionEndpointService sessionEndpointService =
            new SessionEndpointService();

    private static final SessionEndpoint sessionEndpoint =
            sessionEndpointService.getSessionEndpointPort();

    private static final ProjectEndpointService projectEndpointService =
            new ProjectEndpointService();

    private static final ProjectEndpoint projectEndpoint =
            projectEndpointService.getProjectEndpointPort();

    private static SessionDTO session;

    @BeforeClass
    public static void before() {
        session = sessionEndpoint.openSession("test", "test");
    }

    @AfterClass
    public static void after() {
        sessionEndpoint.closeSession(session);
    }

    @Test
    public void testFindAllProject() {
        Assert.assertFalse(projectEndpoint.findProjectAll(session).isEmpty());
        Assert.assertFalse(projectEndpoint.findAllProjectByUserId(session).isEmpty());
        Assert.assertEquals(2, projectEndpoint.sizeProject(session));
        Assert.assertFalse(projectEndpoint.sortedProjectBy(session, "name").isEmpty());
    }

    @Test
    public void testAddFindRemoveProject() {
        final ProjectDTO project = projectEndpoint.addProjectByName(session,"project", "description");
        Assert.assertNotNull(project);
        final String id = project.getId();
        Assert.assertEquals("project", project.getName());
        Assert.assertEquals("description", project.getDescription());
        Assert.assertEquals(Status.NOT_STARTED, project.getStatus());
        ProjectDTO project1 = projectEndpoint.findProjectByNameAndUserId(session, "project");
        Assert.assertNotNull(project1);
        Assert.assertEquals(id, project1.getId());
        Assert.assertEquals("project", project1.getName());
        Assert.assertEquals("description", project1.getDescription());
        project1 = projectEndpoint.findProjectById(session, id);
        Assert.assertNotNull(project1);
        Assert.assertEquals(id, project1.getId());
        Assert.assertEquals("project", project1.getName());
        Assert.assertEquals("description", project1.getDescription());
        projectEndpoint.removeProjectById(session, id);
        Assert.assertNotNull(project);
        Assert.assertNull(projectEndpoint.findProjectById(session,id));
    }

    @Test
    public void testStartFinishProject() {
        final ProjectDTO project = projectEndpoint.addProjectByName(session,"project", "description");
        Assert.assertNotNull(project);
        final String id = project.getId();
        Assert.assertEquals("project", project.getName());
        Assert.assertEquals("description", project.getDescription());
        Assert.assertEquals(Status.NOT_STARTED, project.getStatus());
        ProjectDTO project1 = projectEndpoint.startProjectById(session, id);
        Assert.assertNotNull(project1);
        Assert.assertEquals(id, project1.getId());
        Assert.assertEquals("project", project1.getName());
        Assert.assertEquals("description", project1.getDescription());
        Assert.assertEquals(Status.IN_PROGRESS, project1.getStatus());
        project1 = projectEndpoint.startProjectByName(session, "project");
        Assert.assertNotNull(project1);
        Assert.assertEquals(id, project1.getId());
        Assert.assertEquals("project", project1.getName());
        Assert.assertEquals("description", project1.getDescription());
        Assert.assertEquals(Status.IN_PROGRESS, project1.getStatus());
        project1 = projectEndpoint.finishProjectById(session, id);
        Assert.assertNotNull(project1);
        Assert.assertEquals(id, project1.getId());
        Assert.assertEquals("project", project1.getName());
        Assert.assertEquals("description", project1.getDescription());
        Assert.assertEquals(Status.COMPLETE, project1.getStatus());
        project1 = projectEndpoint.finishProjectByName(session, "project");
        Assert.assertNotNull(project1);
        Assert.assertEquals(id, project1.getId());
        Assert.assertEquals("project", project1.getName());
        Assert.assertEquals("description", project1.getDescription());
        Assert.assertEquals(Status.COMPLETE, project1.getStatus());
        projectEndpoint.removeProjectByName(session, "project");
        Assert.assertNotNull(project);
        Assert.assertNull(projectEndpoint.findProjectById(session,id));
    }

    @Test
    public void testChangeProjectStatus() {
        final ProjectDTO project = projectEndpoint.addProjectByName(session,"project", "description");
        Assert.assertNotNull(project);
        final String id = project.getId();
        Assert.assertEquals("project", project.getName());
        Assert.assertEquals("description", project.getDescription());
        Assert.assertEquals(Status.NOT_STARTED, project.getStatus());
        ProjectDTO project1 = projectEndpoint.changeProjectStatusById(session, id, "IN_PROGRESS");
        Assert.assertNotNull(project1);
        Assert.assertEquals(id, project1.getId());
        Assert.assertEquals("project", project1.getName());
        Assert.assertEquals("description", project1.getDescription());
        Assert.assertEquals(Status.IN_PROGRESS, project1.getStatus());
        project1 = projectEndpoint.changeProjectStatusByName(session, "project", "COMPLETE");
        Assert.assertNotNull(project1);
        Assert.assertEquals(id, project1.getId());
        Assert.assertEquals("project", project1.getName());
        Assert.assertEquals("description", project1.getDescription());
        Assert.assertEquals(Status.COMPLETE, project1.getStatus());
        projectEndpoint.removeProjectByName(session, "project");
        Assert.assertNotNull(project);
        Assert.assertNull(projectEndpoint.findProjectById(session,id));
    }

    @Test
    public void testUpdateProject() {
        final ProjectDTO project = projectEndpoint.addProjectByName(session,"project", "description");
        Assert.assertNotNull(project);
        final String id = project.getId();
        Assert.assertEquals("project", project.getName());
        Assert.assertEquals("description", project.getDescription());
        Assert.assertEquals(Status.NOT_STARTED, project.getStatus());
        Assert.assertNotNull(projectEndpoint.updateProjectById(session, id, "title", "about"));
        Assert.assertNotNull(projectEndpoint.findProjectById(session, id));
        Assert.assertNull(projectEndpoint.findProjectByNameAndUserId(session, "project"));
        final ProjectDTO project1 = projectEndpoint.findProjectByNameAndUserId(session, "title");
        Assert.assertNotNull(project1);
        Assert.assertEquals("about", project1.getDescription());
        projectEndpoint.removeProjectById(session, id);
        Assert.assertNotNull(project);
        Assert.assertNull(projectEndpoint.findProjectById(session,id));
    }

}
