package ru.goloshchapov.tm;

import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import ru.goloshchapov.tm.endpoint.*;
import ru.goloshchapov.tm.service.PropertyService;

import static ru.goloshchapov.tm.util.HashUtil.salt;

public class UserEndpointTest {

    private static final SessionEndpointService sessionEndpointService =
            new SessionEndpointService();

    private static final SessionEndpoint sessionEndpoint =
            sessionEndpointService.getSessionEndpointPort();

    private static final UserEndpointService userEndpointService =
            new UserEndpointService();

    private static final UserEndpoint userEndpoint =
            userEndpointService.getUserEndpointPort();

    private static SessionDTO session;

    @BeforeClass
    public static void before() {
        session = sessionEndpoint.openSession("test", "test");
    }

    @AfterClass
    public static void after() {
        sessionEndpoint.closeSession(session);
    }

    @Test
    public void testGetUser() {
        final UserDTO user = userEndpoint.getUser(session);
        final PropertyService propertyService = new PropertyService();
        Assert.assertNotNull(user);
        Assert.assertEquals("test",user.getLogin());
        final String password = salt(propertyService, "test");
        Assert.assertEquals(password, user.getPasswordHash());
    }

    @Test
    public void testUpdateUser() {
        final UserDTO user = userEndpoint.updateUser(session, "first", "last", "middle");
        Assert.assertNotNull(user);
        Assert.assertEquals("first", user.getFirstname());
        Assert.assertEquals("last", user.getLastname());
        Assert.assertEquals("middle", user.getMiddlename());
    }

    @Test
    public void testSetPassword() {
        final UserDTO user = userEndpoint.setUserPassword(session, "pass");
        Assert.assertNotNull(user);
        final PropertyService propertyService = new PropertyService();
        final String oldPassword = salt(propertyService, "test");
        final String newPassword = salt(propertyService, "pass");
        Assert.assertNotEquals(oldPassword, user.getPasswordHash());
        Assert.assertEquals(newPassword, user.getPasswordHash());
    }
}
