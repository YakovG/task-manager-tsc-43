package ru.goloshchapov.tm.api.entity;

public interface IHaveName {

    String getName();

    void setName(String name);

}
