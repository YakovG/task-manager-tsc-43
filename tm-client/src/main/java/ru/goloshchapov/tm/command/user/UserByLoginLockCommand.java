package ru.goloshchapov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.goloshchapov.tm.command.AbstractCommand;
import ru.goloshchapov.tm.endpoint.SessionDTO;
import ru.goloshchapov.tm.enumerated.Role;
import ru.goloshchapov.tm.util.TerminalUtil;

public final class UserByLoginLockCommand extends AbstractCommand {

    @NotNull public static final String NAME = "user-lock-by-login";

    @NotNull public static final String DESCRIPTION = "Lock user by login";

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return NAME;
    }

    @NotNull
    @Override
    public String description() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        @Nullable SessionDTO session = endpointLocator.getSession();
        System.out.println("[LOCK USER]");
        System.out.println("[ENTER LOGIN]");
        @Nullable final String login = TerminalUtil.nextLine();
        endpointLocator.getAdminEndpoint().lockUserByLogin(session, login);
    }

    @NotNull
    @Override
    public Role[] roles() {
        return new Role[] { Role.ADMIN };
    }
}
