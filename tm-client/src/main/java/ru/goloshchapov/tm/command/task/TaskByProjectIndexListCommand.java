package ru.goloshchapov.tm.command.task;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.goloshchapov.tm.endpoint.SessionDTO;
import ru.goloshchapov.tm.endpoint.TaskDTO;
import ru.goloshchapov.tm.exception.entity.TaskNotFoundException;
import ru.goloshchapov.tm.util.TerminalUtil;

import java.util.List;

public final class TaskByProjectIndexListCommand extends AbstractTaskCommand{

    @NotNull public static final String NAME = "task-list-by-project-index";

    @NotNull public static final String DESCRIPTION = "Show task list by project index";

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return NAME;
    }

    @NotNull
    @Override
    public String description() {
        return DESCRIPTION;
    }

    @Override
    @SneakyThrows
    public void execute() {
        @Nullable SessionDTO session = endpointLocator.getSession();
        System.out.println("[TASK LIST BY PROJECT]");
        System.out.println("ENTER PROJECT INDEX");
        final int projectIndex = TerminalUtil.nextNumber() -1;
        @Nullable final List<TaskDTO> tasks = endpointLocator.getTaskProjectEndpoint().findAllTaskByProjectIndex(session, projectIndex);
        if (tasks == null) throw new TaskNotFoundException();
        int index = 1;
        for (@NotNull final TaskDTO task: tasks) {
            System.out.println(index + ". " + task.getId() + " : " + task.getName());
            index++;
        }
    }
}
