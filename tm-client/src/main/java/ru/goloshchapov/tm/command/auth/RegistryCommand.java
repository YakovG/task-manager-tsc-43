package ru.goloshchapov.tm.command.auth;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.goloshchapov.tm.command.AbstractCommand;
import ru.goloshchapov.tm.endpoint.SessionDTO;
import ru.goloshchapov.tm.util.TerminalUtil;

public final class RegistryCommand extends AbstractCommand {

    @NotNull public static final String NAME = "registry";

    @NotNull public static final String DESCRIPTION = "Registry to system";

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return NAME;
    }

    @NotNull
    @Override
    public String description() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[REGISTRY]");
        System.out.println("[ENTER LOGIN]");
        @Nullable final String login = TerminalUtil.nextLine();
        System.out.println("[ENTER E-MAIL]");
        @Nullable final String email = TerminalUtil.nextLine();
        System.out.println("[ENTER PASSWORD]");
        @Nullable final String password = TerminalUtil.nextLine();
        @Nullable SessionDTO session = endpointLocator.getSession();
        endpointLocator.getAdminEndpoint().createUserWithEmail(session, login, password, email);
    }
}
