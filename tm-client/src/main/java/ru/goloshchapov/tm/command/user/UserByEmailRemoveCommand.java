package ru.goloshchapov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.goloshchapov.tm.command.AbstractCommand;
import ru.goloshchapov.tm.endpoint.SessionDTO;
import ru.goloshchapov.tm.endpoint.UserDTO;
import ru.goloshchapov.tm.enumerated.Role;
import ru.goloshchapov.tm.util.TerminalUtil;

public final class UserByEmailRemoveCommand extends AbstractCommand {

    @NotNull public static final String NAME = "user-remove-by-email";

    @NotNull public static final String DESCRIPTION = "Remove user by email";

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return NAME;
    }

    @NotNull
    @Override
    public String description() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        @Nullable SessionDTO session = endpointLocator.getSession();
        System.out.println("[REMOVE USER]");
        System.out.println("[ENTER EMAIL]");
        @Nullable final String email = TerminalUtil.nextLine();
        @NotNull final UserDTO user = endpointLocator.getAdminEndpoint().removeUserByEmail(session, email);
        final boolean result = endpointLocator.getTaskProjectEndpoint().removeAllTaskByUserId(session);
        if (!result) System.out.println("User with email " + email + "has had an EMPTY project list");
    }

    @NotNull
    @Override
    public Role[] roles() {
        return new Role[] { Role.ADMIN };
    }
}
