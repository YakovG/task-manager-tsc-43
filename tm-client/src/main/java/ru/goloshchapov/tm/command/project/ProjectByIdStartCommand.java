package ru.goloshchapov.tm.command.project;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.goloshchapov.tm.endpoint.ProjectDTO;
import ru.goloshchapov.tm.endpoint.SessionDTO;
import ru.goloshchapov.tm.exception.entity.ProjectNotFoundException;
import ru.goloshchapov.tm.util.TerminalUtil;

public final class ProjectByIdStartCommand extends AbstractProjectCommand{

    @NotNull public static final String NAME = "project-start-by-id";

    @NotNull public static final String DESCRIPTION ="Start project by id";

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return NAME;
    }

    @NotNull
    @Override
    public String description() {
        return DESCRIPTION;
    }

    @Override
    @SneakyThrows
    public void execute() {
        @Nullable SessionDTO session = endpointLocator.getSession();
        System.out.println("[START PROJECT]");
        System.out.println("ENTER ID:");
        @Nullable final String projectId = TerminalUtil.nextLine();
        @Nullable final ProjectDTO project = endpointLocator.getProjectEndpoint().startProjectById(session, projectId);
        if (project == null) throw new ProjectNotFoundException();
    }
}
