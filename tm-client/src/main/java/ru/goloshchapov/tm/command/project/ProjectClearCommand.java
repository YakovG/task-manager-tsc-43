package ru.goloshchapov.tm.command.project;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.goloshchapov.tm.endpoint.ProjectDTO;
import ru.goloshchapov.tm.endpoint.SessionDTO;
import ru.goloshchapov.tm.exception.entity.ProjectListIsEmptyException;

import java.util.List;

public final class ProjectClearCommand extends AbstractProjectCommand{

    @NotNull public static final String NAME = "project-clear";

    @NotNull public static final String DESCRIPTION = "Clear all projects";

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return NAME;
    }

    @NotNull
    @Override
    public String description() {
        return DESCRIPTION;
    }

    @Override
    @SneakyThrows
    public void execute() {
        @Nullable SessionDTO session = endpointLocator.getSession();
        System.out.println("[PROJECT CLEAR]");
        @Nullable final List<ProjectDTO> projects = endpointLocator.getProjectEndpoint().findAllProjectByUserId(session);
        if (projects == null) throw new ProjectListIsEmptyException();
        for (@NotNull final ProjectDTO project: projects) {
            endpointLocator.getTaskProjectEndpoint().removeAllTaskByProjectId(session, project.getId());
        }
        projects.clear();
        endpointLocator.getProjectEndpoint().clearProject(session);
    }
}
