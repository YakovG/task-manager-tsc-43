package ru.goloshchapov.tm.command.auth;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.goloshchapov.tm.command.AbstractCommand;
import ru.goloshchapov.tm.endpoint.SessionDTO;
import ru.goloshchapov.tm.util.TerminalUtil;

public final class PasswordChangeCommand extends AbstractCommand {

    @NotNull public static final String NAME = "password-change";

    @NotNull public static final String DESCRIPTION = "Set new password";

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return NAME;
    }

    @NotNull
    @Override
    public String description() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[CHANGE PASSWORD]");
        System.out.println("ENTER NEW PASSWORD:");
        @Nullable final String password = TerminalUtil.nextLine();
        @Nullable SessionDTO session = endpointLocator.getSession();
        endpointLocator.getUserEndpoint().setUserPassword(session, password);
    }
}
