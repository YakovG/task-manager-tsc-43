package ru.goloshchapov.tm.command.task;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.goloshchapov.tm.endpoint.SessionDTO;
import ru.goloshchapov.tm.endpoint.TaskDTO;
import ru.goloshchapov.tm.exception.entity.TaskNotFoundException;
import ru.goloshchapov.tm.util.TerminalUtil;

public final class TaskByNameStartCommand extends AbstractTaskCommand{

    @NotNull public static final String NAME = "task-start-by-name";

    @NotNull public static final String DESCRIPTION = "Start task by name";

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return NAME;
    }

    @NotNull
    @Override
    public String description() {
        return DESCRIPTION;
    }

    @Override
    @SneakyThrows
    public void execute() {
        @Nullable SessionDTO session = endpointLocator.getSession();
        System.out.println("[START TASK]");
        System.out.println("ENTER NAME:");
        @Nullable final String name = TerminalUtil.nextLine();
        @Nullable final TaskDTO task = endpointLocator.getTaskEndpoint().startTaskByName(session, name);
        if (task == null) throw new TaskNotFoundException();
    }
}
