package ru.goloshchapov.tm.command.project;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.goloshchapov.tm.endpoint.ProjectDTO;
import ru.goloshchapov.tm.endpoint.SessionDTO;
import ru.goloshchapov.tm.exception.entity.ProjectNotFoundException;
import ru.goloshchapov.tm.exception.entity.ProjectNotUpdatedException;
import ru.goloshchapov.tm.util.TerminalUtil;

public final class ProjectByIdUpdateCommand extends AbstractProjectCommand{

    @NotNull public static final String NAME = "project-update-by-id";

    @NotNull public static final String DESCRIPTION ="Update project by id";

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return NAME;
    }

    @NotNull
    @Override
    public String description() {
        return DESCRIPTION;
    }

    @Override
    @SneakyThrows
    public void execute() {
        @Nullable SessionDTO session = endpointLocator.getSession();
        System.out.println("[UPDATE PROJECT]");
        System.out.println("ENTER ID:");
        @Nullable final String projectId = TerminalUtil.nextLine();
        @Nullable final ProjectDTO project = endpointLocator.getProjectEndpoint().findProjectById(session, projectId);
        if (project == null) throw new ProjectNotFoundException();
        System.out.println("ENTER NAME:");
        @Nullable final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        @Nullable final String description = TerminalUtil.nextLine();
        @Nullable final ProjectDTO projectUpdated = endpointLocator.getProjectEndpoint()
                .updateProjectById(session, projectId,name,description);
        if (projectUpdated == null) throw new ProjectNotUpdatedException();
    }
}
