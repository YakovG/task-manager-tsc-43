package ru.goloshchapov.tm.command.project;

import org.jetbrains.annotations.Nullable;
import ru.goloshchapov.tm.command.AbstractCommand;
import ru.goloshchapov.tm.endpoint.ProjectDTO;
import ru.goloshchapov.tm.endpoint.SessionDTO;

public abstract class AbstractProjectCommand extends AbstractCommand {

    protected void showProject(@Nullable final ProjectDTO project) {
        @Nullable SessionDTO session = endpointLocator.getSession();
        endpointLocator.getProjectEndpoint().checkProjectAccess(session);
        if (project == null) return;
        System.out.println("ID: " + project.getId());
        System.out.println("NAME: " + project.getName());
        System.out.println("DESCRIPTION: " + project.getDescription());
        System.out.println("STATUS: " + project.getStatus());
        System.out.println("CREATED: " + project.getCreated());
        if (project.getDateStart() != null) System.out.println("STARTED: " + project.getDateStart());
        if (project.getDateFinish() != null) System.out.println("FINISHED: " + project.getDateFinish());
    }

}
