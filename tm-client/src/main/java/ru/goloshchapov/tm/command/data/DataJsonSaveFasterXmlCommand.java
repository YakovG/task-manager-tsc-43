package ru.goloshchapov.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.goloshchapov.tm.command.AbstractCommand;
import ru.goloshchapov.tm.endpoint.SessionDTO;


public final class DataJsonSaveFasterXmlCommand extends AbstractCommand {

    @NotNull
    public static final String NAME = "data-json-fasterxml-save";

    @NotNull public static final String DESCRIPTION = "Save data to JSON (FASTERXML)";

    @Override
    public @Nullable String arg() {
        return null;
    }

    @Override
    public @NotNull String name() {
        return NAME;
    }

    @Override
    public @NotNull String description() {
        return DESCRIPTION;
    }

    @Override
    @SneakyThrows
    public void execute() {
        @Nullable SessionDTO session = endpointLocator.getSession();
        System.out.println("[DATA SAVE JSON]");
        endpointLocator.getAdminEndpoint().saveJsonFasterXmlData(session);
    }

}
