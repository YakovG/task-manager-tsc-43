package ru.goloshchapov.tm.command.project;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.goloshchapov.tm.endpoint.ProjectDTO;
import ru.goloshchapov.tm.endpoint.SessionDTO;
import ru.goloshchapov.tm.enumerated.Status;
import ru.goloshchapov.tm.exception.entity.ProjectNotUpdatedException;
import ru.goloshchapov.tm.util.TerminalUtil;

import java.util.Arrays;

public final class ProjectByIdChangeStatusCommand extends AbstractProjectCommand{

    @NotNull public static final String NAME = "project-change-status-by-id";

    @NotNull public static final String DESCRIPTION ="Change project status by id";

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return NAME;
    }

    @NotNull
    @Override
    public String description() {
        return DESCRIPTION;
    }

    @Override
    @SneakyThrows
    public void execute() {
        @Nullable SessionDTO session = endpointLocator.getSession();
        System.out.println("[CHANGE PROJECT STATUS]");
        System.out.println("ENTER ID:");
        @Nullable final String projectId = TerminalUtil.nextLine();
        System.out.println("ENTER STATUS:");
        @NotNull final Status[] statuses = Status.values();
        System.out.println(Arrays.toString(statuses));
        @Nullable final String statusChange = TerminalUtil.nextLine();
        @Nullable final ProjectDTO project = endpointLocator.getProjectEndpoint()
                .changeProjectStatusById(session, projectId, statusChange);
        if (project == null) throw new ProjectNotUpdatedException();
    }
}
