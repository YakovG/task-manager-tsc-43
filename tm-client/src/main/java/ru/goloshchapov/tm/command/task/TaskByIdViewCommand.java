package ru.goloshchapov.tm.command.task;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.goloshchapov.tm.endpoint.SessionDTO;
import ru.goloshchapov.tm.endpoint.TaskDTO;
import ru.goloshchapov.tm.exception.entity.TaskNotFoundException;
import ru.goloshchapov.tm.util.TerminalUtil;

public final class TaskByIdViewCommand extends AbstractTaskCommand{

    @NotNull public static final String NAME = "task-view-by-id";

    @NotNull public static final String DESCRIPTION = "Show task by id";

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return NAME;
    }

    @NotNull
    @Override
    public String description() {
        return DESCRIPTION;
    }

    @Override
    @SneakyThrows
    public void execute() {
        @Nullable SessionDTO session = endpointLocator.getSession();
        System.out.println("[SHOW TASK]");
        System.out.println("ENTER ID:");
        @Nullable final String taskId = TerminalUtil.nextLine();
        @Nullable final TaskDTO task = endpointLocator.getTaskEndpoint().findTaskById(session, taskId);
        if (task == null) throw new TaskNotFoundException();
        showTask(task);
    }
}
