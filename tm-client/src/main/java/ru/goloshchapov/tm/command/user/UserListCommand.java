package ru.goloshchapov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.goloshchapov.tm.command.AbstractCommand;
import ru.goloshchapov.tm.endpoint.SessionDTO;
import ru.goloshchapov.tm.endpoint.UserDTO;

import java.util.List;

import static ru.goloshchapov.tm.util.ValidationUtil.isEmpty;

public final class UserListCommand extends AbstractCommand {

    @NotNull public static final String NAME = "user-list";

    @NotNull public static final String DESCRIPTION = "Show user list";

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return NAME;
    }

    @NotNull
    @Override
    public String description() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        @Nullable SessionDTO session = endpointLocator.getSession();
        System.out.println("[USER LIST]");
        endpointLocator.getAdminEndpoint().showUserList(session);
        @Nullable final List<UserDTO> users = endpointLocator.getAdminEndpoint().findUserAll(session);
        for (@NotNull final UserDTO user:users) {
            if (user.isLocked()) System.out.print("LOCKED! ");
            System.out.print(user.getLogin() + " ");
            if (!isEmpty(user.getEmail())) System.out.print(user.getEmail() + " ");
            System.out.println(user.getRole());
        }
    }

}
