package ru.goloshchapov.tm.command.task;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.goloshchapov.tm.endpoint.SessionDTO;
import ru.goloshchapov.tm.endpoint.TaskDTO;
import ru.goloshchapov.tm.exception.entity.TaskNotFoundException;
import ru.goloshchapov.tm.util.TerminalUtil;

public final class TaskByProjectIdUnbindCommand extends AbstractTaskCommand{

    @NotNull public static final String NAME = "task-unbind-from-project-by-id";

    @NotNull public static final String DESCRIPTION = "Remove task from project by id";

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return NAME;
    }

    @NotNull
    @Override
    public String description() {
        return DESCRIPTION;
    }

    @Override
    @SneakyThrows
    public void execute() {
        @Nullable SessionDTO session = endpointLocator.getSession();
        System.out.println("REMOVE TASK FROM PROJECT");
        System.out.println("ENTER TASK ID");
        @Nullable final String taskId = TerminalUtil.nextLine();
        System.out.println("ENTER PROJECT ID");
        @Nullable final String projectId = TerminalUtil.nextLine();
        @Nullable final TaskDTO task =
                endpointLocator.getTaskProjectEndpoint().unbindTaskFromProjectById(session, taskId, projectId);
        if (task == null) throw new TaskNotFoundException();
    }
}
