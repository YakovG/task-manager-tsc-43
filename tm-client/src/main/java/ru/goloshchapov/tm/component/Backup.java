package ru.goloshchapov.tm.component;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.goloshchapov.tm.bootstrap.Bootstrap;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public final class Backup implements Runnable{

    @NotNull
    private final ScheduledExecutorService es = Executors.newSingleThreadScheduledExecutor();

    @NotNull
    private static final String COMMAND_SAVE = "backup-save";

    @NotNull
    private static final String COMMAND_LOAD = "backup-load";

    @NotNull
    private static final String COMMAND_CLEAR = "backup-clear";

    private static final int INTERVAL = 30;

    @NotNull
    private final Bootstrap bootstrap;

    public Backup(@NotNull final Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
    }

    public void init() {
        start();
    }

    public void start() {
        es.scheduleWithFixedDelay(this, 0, INTERVAL, TimeUnit.SECONDS);
    }

    public void stop() {
        es.shutdown();
    }

    @SneakyThrows
    public void run() {
        bootstrap.parseCommand(COMMAND_SAVE);
    }

    @SneakyThrows
    public void load() {
        bootstrap.parseCommand(COMMAND_LOAD);
    }

    @SneakyThrows
    public void delete() { bootstrap.parseCommand(COMMAND_CLEAR);}

}
