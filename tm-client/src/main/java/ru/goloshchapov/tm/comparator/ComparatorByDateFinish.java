package ru.goloshchapov.tm.comparator;

import ru.goloshchapov.tm.api.entity.IHaveDateFinish;

import java.util.Comparator;

public final class ComparatorByDateFinish implements Comparator<IHaveDateFinish> {

    private static final ComparatorByDateFinish INSTANCE = new ComparatorByDateFinish();

    private ComparatorByDateFinish() {
    }

    public static ComparatorByDateFinish getInstance() {
        return INSTANCE;
    }

    @Override
    public int compare(final IHaveDateFinish o1, final IHaveDateFinish o2) {
        if (o1 == null || o2 == null) return 0;
        return o1.getDateFinish().compareTo(o2.getDateFinish());
    }

}
