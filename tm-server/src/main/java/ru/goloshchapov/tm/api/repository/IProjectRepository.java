package ru.goloshchapov.tm.api.repository;

import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.goloshchapov.tm.dto.ProjectDTO;

import java.util.List;

public interface IProjectRepository {

    @Insert("INSERT INTO `tm_project` (`id`, `name`, `description`, `user_id`, `status`, " +
            "`created`, `start_date`, `finish_date`) " +
            "VALUES(#{id}, #{name}, #{description}, #{userId}, #{status}, #{created}, #{dateStart}, #{dateFinish})")
    void add(@NotNull ProjectDTO project);

    @Update("UPDATE `tm_project` SET `name` = #{name}, `description` = #{description}, `user_id` = #{userId}, " +
            "`status` = #{status}, `created` = #{created}, `start_date` = #{dateStart}, " +
            "`finish_date` = #{dateFinish} WHERE `id` = #{id}")
    void update(@NotNull ProjectDTO project);

    @Nullable
    @Select("SELECT * FROM `tm_project`")
    @Result(column = "user_id", property = "userId")
    @Result(column = "start_date", property = "dateStart")
    @Result(column = "finish_date", property = "dateFinish")
    List<ProjectDTO> findAllProjects();

    @Nullable
    @Select("SELECT * FROM `tm_project` WHERE `user_id` = #{userId}")
    @Result(column = "user_id", property = "userId")
    @Result(column = "start_date", property = "dateStart")
    @Result(column = "finish_date", property = "dateFinish")
    List<ProjectDTO> findAll(@NotNull String userId);

    @Nullable
    @Select("SELECT * FROM `tm_project` WHERE `user_id` = #{userId}")
    @Result(column = "user_id", property = "userId")
    @Result(column = "start_date", property = "dateStart")
    @Result(column = "finish_date", property = "dateFinish")
    List<ProjectDTO> findAllByUserId(@NotNull final String userId);

    @Nullable
    @Select("SELECT * FROM `tm_project` WHERE `id` = #{projectId} AND `user_id` = #{userId} LIMIT 1")
    @Result(column = "user_id", property = "userId")
    @Result(column = "start_date", property = "dateStart")
    @Result(column = "finish_date", property = "dateFinish")
    ProjectDTO findOneById(
            @NotNull final @Param("userId") String userId,
            @NotNull final @Param("projectId") String projectId
    );

    @Nullable
    @Select("SELECT * FROM `tm_project` WHERE `id` = #{projectId} LIMIT 1")
    @Result(column = "user_id", property = "userId")
    @Result(column = "start_date", property = "dateStart")
    @Result(column = "finish_date", property = "dateFinish")
    ProjectDTO findProjectById(@NotNull final @Param("projectId") String projectId);

    @Nullable
    @Select("SELECT * FROM `tm_project` WHERE `name` = #{name} LIMIT 1")
    @Result(column = "user_id", property = "userId")
    @Result(column = "start_date", property = "dateStart")
    @Result(column = "finish_date", property = "dateFinish")
    ProjectDTO findProjectByName(@NotNull final @Param("name") String name);

    @Nullable
    @Select("SELECT * FROM `tm_project` WHERE `name` = #{name} AND `user_id` = #{userId} LIMIT 1")
    @Result(column = "user_id", property = "userId")
    @Result(column = "start_date", property = "dateStart")
    @Result(column = "finish_date", property = "dateFinish")
    ProjectDTO findOneByName(
            @NotNull final @Param("userId") String userId,
            @NotNull final @Param("name") String name);

    @Delete("DELETE FROM `tm_project` WHERE `id` = #{projectId} AND `user_id` = #{userId}")
    void removeOneById(@NotNull final String userId, @NotNull final String projectId);

    @Delete("DELETE FROM `tm_project` WHERE `id` = #{projectId}")
    void removeProjectById(@NotNull final String projectId);

    @Delete("DELETE FROM `tm_project` WHERE `user_id` = #{userId}")
    void clear(@NotNull final String userId);

    @Delete("DELETE FROM `tm_project`")
    void clearAll();

    @Select("SELECT COUNT(*) FROM `tm_project`")
    int size();
}
