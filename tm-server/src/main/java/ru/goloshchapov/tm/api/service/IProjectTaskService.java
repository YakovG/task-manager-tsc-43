package ru.goloshchapov.tm.api.service;

import org.jetbrains.annotations.Nullable;
import ru.goloshchapov.tm.dto.ProjectDTO;
import ru.goloshchapov.tm.dto.TaskDTO;

import java.util.List;

public interface IProjectTaskService {

    @Nullable
    ProjectDTO addProject(String userId, String projectName, String projectDescription);

    @Nullable
    TaskDTO addTask(String userId, String taskName, String taskDescription);

    void clearProjects(String userId);

    @Nullable
    List<ProjectDTO> findAllProjects(String userId);

    @Nullable
    List<TaskDTO> findAllByProjectId(String userId, String projectId);

    boolean isEmptyProjectById(String userId, String projectId);

    @Nullable
    List<TaskDTO> findAllByProjectName(String userId, String projectName);

    boolean isEmptyProjectByName(String userId, String projectName);

    @Nullable
    List<TaskDTO> findAllByProjectIndex(String userId, Integer projectIndex);

    boolean isEmptyProjectByIndex(String userId, Integer projectIndex);

    @Nullable
    TaskDTO bindToProjectById(String userId, String taskId, String projectId);

    @Nullable
    TaskDTO unbindFromProjectById(String userId, String taskId, String projectId);

    @Nullable
    List<TaskDTO> removeAllByProjectId(String userId, String projectId);

    @Nullable
    List<TaskDTO> removeAllByProjectName(String userId, String projectName);

    @Nullable
    List<TaskDTO> removeAllByProjectIndex(String userId, Integer projectIndex);

    boolean removeAllByUserId(String userId);

    @Nullable
    ProjectDTO removeProjectById(String userId, String projectId);

    @Nullable
    ProjectDTO removeProjectByName(String userId, String projectName);

    @Nullable
    ProjectDTO removeProjectByIndex(String userId, Integer projectIndex);

    void createTestData();

}
