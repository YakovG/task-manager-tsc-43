package ru.goloshchapov.tm.api.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.goloshchapov.tm.dto.ProjectDTO;

import java.util.List;

public interface IProjectRepositoryDTO {

    void add(@NotNull ProjectDTO project);

    void update(@NotNull ProjectDTO project);

    @Nullable
    List<ProjectDTO> findAllProjects();

    @Nullable
    List<ProjectDTO> findAll(@NotNull String userId);

    @Nullable
    List<ProjectDTO> findAllByUserId(@NotNull final String userId);

    @Nullable
    ProjectDTO findOneById(
            @NotNull final String userId,
            @NotNull final String projectId
    );

    @Nullable
    ProjectDTO findProjectById(@NotNull final String id);

    @Nullable
    ProjectDTO findProjectByName(@NotNull final String name);

    @Nullable
    ProjectDTO findOneByName(
            @NotNull final String userId,
            @NotNull final String name);

    void removeOneById(@NotNull final String userId, @NotNull final String projectId);

    void removeProjectById(@NotNull final String id);

    void clear(@NotNull final String userId);

    void clearAll();

    int size();
}
