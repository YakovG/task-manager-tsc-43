package ru.goloshchapov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.goloshchapov.tm.dto.UserDTO;
import ru.goloshchapov.tm.enumerated.Role;

public interface IAuthService {

    @NotNull String getUserId();

    @Nullable UserDTO getUser();

    boolean isAuth();

    void checkRoles(Role... roles);

    void logout();

    void login(String login, String password);

    void registry (String login, String password, String email);

    void registry (String login,
                   String password,
                   String email,
                   String role
    );

}
