package ru.goloshchapov.tm.api.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.goloshchapov.tm.dto.UserDTO;

import java.util.List;

public interface IUserRepositoryDTO {

    void add(UserDTO user);

    @Nullable
    List<UserDTO> findAllUsers();

    @Nullable
    UserDTO findUserById(@NotNull String id);

    @Nullable
    UserDTO findUserByLogin(@Nullable String login);

    @Nullable
    UserDTO findUserByEmail(@Nullable String email);

    void removeUserById(@Nullable String id);

    void update(@NotNull UserDTO user);

    void clear();

    int size();

}
