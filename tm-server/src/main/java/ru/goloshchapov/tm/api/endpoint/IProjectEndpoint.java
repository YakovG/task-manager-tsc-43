package ru.goloshchapov.tm.api.endpoint;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.goloshchapov.tm.dto.ProjectDTO;
import ru.goloshchapov.tm.dto.SessionDTO;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import java.util.Collection;
import java.util.List;

public interface IProjectEndpoint {
    @WebMethod
    @SneakyThrows
    @Nullable List<ProjectDTO> findProjectAll(
            @WebParam(name = "session") SessionDTO session);

    @WebMethod
    @SneakyThrows
    void addProjectAll(
            @WebParam(name = "session") SessionDTO session,
            @WebParam(name = "collection") Collection<ProjectDTO> collection
    );

    @Nullable
    @WebMethod
    @SneakyThrows
    ProjectDTO addProject(
            @WebParam(name = "session") SessionDTO session,
            @WebParam(name = "project") ProjectDTO model
    );

    @Nullable
    @WebMethod
    @SneakyThrows
    List<ProjectDTO> findAllProjectByUserId(
            @WebParam(name = "session") SessionDTO session
    );

    @Nullable
    @WebMethod
    @SneakyThrows
    ProjectDTO findProjectById(
            @WebParam(name = "session") SessionDTO session,
            @WebParam(name = "projectId") String modelId
    );

    @Nullable
    @WebMethod
    @SneakyThrows
    ProjectDTO findProjectByIndex(
            @WebParam(name = "session") SessionDTO session,
            @WebParam(name = "index") Integer index
    );

    @Nullable
    @WebMethod
    @SneakyThrows
    ProjectDTO findProjectByNameAndUserId(
            @WebParam(name = "session") SessionDTO session,
            @WebParam(name = "name") String name);

    @WebMethod
    @SneakyThrows
    int sizeProject(
            @WebParam(name = "session") SessionDTO session
    );

    @WebMethod
    @SneakyThrows
    void removeProject(
            @WebParam(name = "session") SessionDTO session,
            @WebParam(name = "name") ProjectDTO model
    );

    @WebMethod
    @SneakyThrows
    void clearProject(
            @WebParam(name = "session") SessionDTO session
    );

    @Nullable
    @WebMethod
    @SneakyThrows
    ProjectDTO removeProjectById(
            @WebParam(name = "session") SessionDTO session,
            @WebParam(name = "projectId") String modelId
    );

    @Nullable
    @WebMethod
    @SneakyThrows
    ProjectDTO removeProjectByIndex(
            @WebParam(name = "session") SessionDTO session,
            @WebParam(name = "index") Integer index
    );

    @Nullable
    @WebMethod
    @SneakyThrows
    ProjectDTO removeProjectByName(
            @WebParam(name = "session") SessionDTO session,
            @WebParam(name = "name") String name
    );

    @Nullable
    @WebMethod
    @SneakyThrows
    ProjectDTO startProjectById(
            @WebParam(name = "session") SessionDTO session,
            @WebParam(name = "projectId") String modelId
    );

    @Nullable
    @WebMethod
    @SneakyThrows
    ProjectDTO startProjectByIndex(
            @WebParam(name = "session") SessionDTO session,
            @WebParam(name = "index") Integer index
    );

    @Nullable
    @WebMethod
    @SneakyThrows
    ProjectDTO startProjectByName(
            @WebParam(name = "session") SessionDTO session,
            @WebParam(name = "name") String name
    );

    @Nullable
    @WebMethod
    @SneakyThrows
    ProjectDTO finishProjectById(
            @WebParam(name = "session") SessionDTO session,
            @WebParam(name = "projectId") String modelId
    );

    @Nullable
    @WebMethod
    @SneakyThrows
    ProjectDTO finishProjectByIndex(
            @WebParam(name = "session") SessionDTO session,
            @WebParam(name = "index") Integer index
    );

    @Nullable
    @WebMethod
    @SneakyThrows
    ProjectDTO finishProjectByName(
            @WebParam(name = "session") SessionDTO session,
            @WebParam(name = "name") String name
    );

    @Nullable
    @WebMethod
    @SneakyThrows
    List<ProjectDTO> sortedProjectBy(
            @WebParam(name = "session") SessionDTO session,
            @WebParam(name = "sortCheck") String sortCheck
    );

    @Nullable
    @WebMethod
    @SneakyThrows
    ProjectDTO updateProjectById(
            @WebParam(name = "session") SessionDTO session,
            @WebParam(name = "projectId") String modelId,
            @WebParam(name = "name") String name,
            @WebParam(name = "description") String description
    );

    @Nullable
    @WebMethod
    @SneakyThrows
    ProjectDTO updateProjectByIndex(@WebParam(name = "session") SessionDTO session,
                                    @WebParam(name = "index") Integer index,
                                    @WebParam(name = "name") String name,
                                    @WebParam(name = "description") String description
    );

    @Nullable
    @WebMethod
    @SneakyThrows
    ProjectDTO changeProjectStatusById(@WebParam(name = "session") SessionDTO session,
                                       @WebParam(name = "projectId") String id,
                                       @WebParam(name = "statusChange") String statusChange
    );

    @Nullable
    @WebMethod
    @SneakyThrows
    ProjectDTO changeProjectStatusByName(@WebParam(name = "session") SessionDTO session,
                                         @WebParam(name = "name") String name,
                                         @WebParam(name = "statusChange") String statusChange
    );

    @Nullable
    @WebMethod
    @SneakyThrows
    ProjectDTO changeProjectStatusByIndex(@WebParam(name = "session") SessionDTO session,
                                          @WebParam(name = "index") int index,
                                          @WebParam(name = "statusChange") String statusChange
    );

    @WebMethod
    @SneakyThrows
    @NotNull ProjectDTO addProjectByName(@WebParam(name = "session") SessionDTO session,
                                         @WebParam(name = "name") String name,
                                         @WebParam(name = "description") String description
    );

    @WebMethod
    @SneakyThrows
    void checkProjectAccess(
            @WebParam(name = "session") SessionDTO session
    );
}
