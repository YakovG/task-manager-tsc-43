package ru.goloshchapov.tm.api.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.goloshchapov.tm.api.IBusinessService;
import ru.goloshchapov.tm.dto.ProjectDTO;

import java.util.Collection;
import java.util.List;

public interface IProjectService extends IBusinessService<ProjectDTO> {

    @Nullable
    @Override
    @SneakyThrows
    ProjectDTO add(@Nullable ProjectDTO project);

    @Nullable
    @SneakyThrows
    ProjectDTO add(@Nullable String userId, @Nullable ProjectDTO project);

    @Nullable
    ProjectDTO add(
            @Nullable String userId,
            @Nullable String name,
            @Nullable String description
    );

    @SneakyThrows
    void addAll(@Nullable String userId, @Nullable Collection<ProjectDTO> collection);

    @Nullable
    @SneakyThrows
    ProjectDTO update(@Nullable ProjectDTO project);

    @Nullable
    @SneakyThrows
    List<ProjectDTO> findAllByUserId(@Nullable String userId);

    @Nullable
    @Override
    @SneakyThrows
    List<ProjectDTO> findAll();

    @Nullable
    @SneakyThrows
    List<ProjectDTO> findAll(@Nullable String userId);

    @Nullable
    @SneakyThrows
    ProjectDTO findOneById(@Nullable String userId, @Nullable String projectId);

    @SneakyThrows
    boolean isAbsentById(@NotNull String userId, @NotNull String projectId);

    @SneakyThrows
    boolean isAbsentByName(@NotNull String userId, @NotNull String projectName);

    @Nullable
    @SneakyThrows
    ProjectDTO findOneByName(@Nullable String name);

    @Nullable
    @SneakyThrows
    ProjectDTO findOneByName(@Nullable String userId, @Nullable String name);

    @Nullable
    @SneakyThrows
    ProjectDTO removeOneById(@Nullable String userId, @Nullable String projectId);

    @Nullable
    @SneakyThrows
    ProjectDTO removeOneById(@Nullable String projectId);

    @SneakyThrows
    void clear(@Nullable String userId);
}
