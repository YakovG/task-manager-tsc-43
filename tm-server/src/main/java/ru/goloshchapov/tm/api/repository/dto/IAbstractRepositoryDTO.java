package ru.goloshchapov.tm.api.repository.dto;

import org.jetbrains.annotations.Nullable;
import ru.goloshchapov.tm.dto.AbstractEntityDTO;

import java.util.Collection;

public interface IAbstractRepositoryDTO<E extends AbstractEntityDTO> {

    void add(E entity);

    void addAll(@Nullable Collection<E> collection);

    void update(E entity);

    void remove(E entity);

}
