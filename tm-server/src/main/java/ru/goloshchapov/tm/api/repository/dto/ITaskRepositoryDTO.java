package ru.goloshchapov.tm.api.repository.dto;

import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.goloshchapov.tm.dto.TaskDTO;

import java.util.List;

public interface ITaskRepositoryDTO {

    void add(@NotNull TaskDTO task);

    void update(@NotNull TaskDTO task);

    @Nullable
    List<TaskDTO> findAllTasks();

    @Nullable
    List<TaskDTO> findAll(@NotNull String userId);

    @Nullable
    List<TaskDTO> findAllByUserId(@NotNull final String userId);

    @Nullable
    TaskDTO findOneById(
            @NotNull final String userId,
            @NotNull final String id
    );

    @Nullable
    TaskDTO findTaskById(@NotNull final String id);

    @Nullable
    TaskDTO findTaskByName(@NotNull final String name);

    @Nullable
    TaskDTO findOneByName(
            @NotNull final String userId,
            @NotNull final String name
    );

    void removeOneById(@NotNull final String userId, @NotNull final String taskId);

    void removeTaskById(@NotNull final String taskId);

    void clear(@NotNull final String userId);

    void clearAll();

    int size();

    @Nullable
    List<TaskDTO> findAllByProjectId(
            @Nullable final String userId,
            @Nullable final String projectId
    );

    void bindToProjectById (
            @Nullable final String userId,
            @Nullable final String taskId,
            @Nullable final String projectId
    );

    void unbindFromProjectById (
            @Nullable final String userId,
            @Nullable final String taskId,
            @Nullable final String projectId
    );

    void removeAllByProjectId(
            @Nullable final String userId,
            @Nullable final String projectId
    );
}
