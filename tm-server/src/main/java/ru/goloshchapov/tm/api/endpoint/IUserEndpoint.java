package ru.goloshchapov.tm.api.endpoint;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.goloshchapov.tm.dto.SessionDTO;
import ru.goloshchapov.tm.dto.UserDTO;

import javax.jws.WebMethod;
import javax.jws.WebParam;

public interface IUserEndpoint {
    @WebMethod
    @SneakyThrows
    @NotNull UserDTO setUserPassword(
            @WebParam(name = "session") SessionDTO session,
            @WebParam(name = "password") String password
    );

    @WebMethod
    @SneakyThrows
    @NotNull UserDTO updateUser(
            @WebParam(name = "session") SessionDTO session,
            @WebParam(name = "firstName") String firstName,
            @WebParam(name = "lastName") String lastName,
            @WebParam(name = "middleName") String middleName
    );

    UserDTO getUser(
            @WebParam(name = "session") SessionDTO session
    );
}
