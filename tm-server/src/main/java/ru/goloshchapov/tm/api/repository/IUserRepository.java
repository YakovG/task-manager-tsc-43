package ru.goloshchapov.tm.api.repository;


import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.goloshchapov.tm.dto.UserDTO;

import java.util.List;

public interface IUserRepository {

    @Insert("INSERT INTO `tm_user` " +
            "(`id`,`login`,`password_hash`,`email`,`first_name`,`last_name`,`middle_name`,`role`,`locked`) " +
            "VALUES(#{id}, #{login}, #{passwordHash}, #{email}, " +
            "#{firstname}, #{lastname}, #{middlename}, #{role}, #{locked})")
    void add(UserDTO user);

    @Nullable
    @Select("SELECT * FROM `tm_user`")
    @Result(column = "password_hash", property = "passwordHash")
    @Result(column = "first_name", property = "firstname")
    @Result(column = "last_name", property = "lastname")
    @Result(column = "middle_name", property = "middlename")
    List<UserDTO> findAllUsers();

    @Nullable
    @Select("SELECT * FROM `tm_user` WHERE `id` = #{id} LIMIT 1")
    @Result(column = "password_hash", property = "passwordHash")
    @Result(column = "first_name", property = "firstname")
    @Result(column = "last_name", property = "lastname")
    @Result(column = "middle_name", property = "middlename")
    UserDTO findUserById(@NotNull String id);

    @Nullable
    @Select("SELECT * FROM `tm_user` WHERE `login` = #{login} LIMIT 1")
    @Result(column = "password_hash", property = "passwordHash")
    @Result(column = "first_name", property = "firstname")
    @Result(column = "last_name", property = "lastname")
    @Result(column = "middle_name", property = "middlename")
    UserDTO findUserByLogin(@Nullable String login);

    @Nullable
    @Select("SELECT * FROM `tm_user` WHERE `email` = #{email} LIMIT 1")
    @Result(column = "password_hash", property = "passwordHash")
    @Result(column = "first_name", property = "firstname")
    @Result(column = "last_name", property = "lastname")
    @Result(column = "middle_name", property = "middlename")
    UserDTO findUserByEmail(@Nullable String email);

    @Nullable
    @Delete("DELETE FROM `tm_user` WHERE `id` = #{userId}")
    void removeUserById(@Nullable String userId);

    @Nullable
    @Update("UPDATE `tm_user` SET `login` = #{login}, `password_hash` = #{passwordHash}, `email` = #{email}, " +
            "`first_name` = #{firstname}, `last_name` = #{lastname}, `middle_name` = #{middlename}, " +
            "`role` = #{role}, `locked` = #{locked} WHERE `id` = #{id}")
    void update(@NotNull UserDTO user);

    @Delete("DELETE FROM `tm_user`")
    void clear();

    @Select("SELECT COUNT(*) FROM `tm_user`")
    int size();

}
