package ru.goloshchapov.tm.api.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.goloshchapov.tm.dto.SessionDTO;

import java.util.List;

public interface ISessionRepositoryDTO {

    void add (@NotNull SessionDTO session);

    int contains(@Nullable String sessionId);

    void remove(@NotNull String sessionId);

    @Nullable
    SessionDTO findByUserId (@Nullable String userId);

    @Nullable
    List<SessionDTO> findAllSession();

    void clear();

    @Nullable
    SessionDTO findSessionById(@Nullable final String id);
}
