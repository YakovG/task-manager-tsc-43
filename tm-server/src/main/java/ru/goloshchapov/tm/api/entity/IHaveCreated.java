package ru.goloshchapov.tm.api.entity;

import java.util.Date;

public interface IHaveCreated {

    Date getCreated ();

    void setCreated (Date date);

}
