package ru.goloshchapov.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.goloshchapov.tm.api.repository.dto.ISessionRepositoryDTO;
import ru.goloshchapov.tm.dto.SessionDTO;

import javax.persistence.EntityManager;
import java.util.List;

public class SessionRepositoryDTO extends AbstractRepositoryDTO<SessionDTO> implements ISessionRepositoryDTO {

    public SessionRepositoryDTO(@NotNull EntityManager entityManager) {
        super(entityManager);
    }

    @Nullable
    public SessionDTO findSessionById(@Nullable final String id) {
        return entityManager.find(SessionDTO.class, id);
    }

    @Nullable
    public List<SessionDTO> findAllSession(){
        return entityManager.createQuery("SELECT e FROM SessionDTO e", SessionDTO.class).getResultList();
    }

    @Nullable
    public SessionDTO findByUserId (@Nullable final String userId) {
        List<SessionDTO> list = entityManager
                .createQuery("SELECT e FROM SessionDTO e WHERE e.userId = :userId", SessionDTO.class)
                .setParameter("userId", userId)
                .setMaxResults(1).getResultList();
        if (list.isEmpty())
            return null;
        else
            return list.get(0);
    }

    public void remove(@NotNull String sessionId) {
        SessionDTO reference = entityManager.getReference(SessionDTO.class, sessionId);
        entityManager.remove(reference);
    }

    public void clear() {
        entityManager.createQuery("DELETE FROM SessionDTO e").executeUpdate();
    }

    public int contains(@Nullable final String sessionId) {
        return entityManager
                .createQuery("SELECT COUNT(e) FROM SessionDTO e WHERE e.id = :sessionId", Integer.class)
                .setParameter("sessionId", sessionId)
                .getSingleResult();
    }
}
