package ru.goloshchapov.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.goloshchapov.tm.api.repository.dto.IUserRepositoryDTO;
import ru.goloshchapov.tm.dto.UserDTO;

import javax.persistence.EntityManager;
import java.util.List;

public class UserRepositoryDTO extends AbstractRepositoryDTO<UserDTO> implements IUserRepositoryDTO {

    public UserRepositoryDTO(@NotNull EntityManager entityManager) {
        super(entityManager);
    }

    @Nullable
    public UserDTO findUserById(@NotNull String id) {
        return entityManager.find(UserDTO.class, id);
    }

    @Nullable
    public List<UserDTO> findAllUsers() {
        return entityManager.createQuery( "SELECT e FROM UserDTO e", UserDTO.class).getResultList();
    }

    @Nullable
    public UserDTO findUserByLogin(@Nullable String login) {
        List<UserDTO> list = entityManager
                .createQuery("SELECT e FROM SessionDTO e WHERE e.login = :login", UserDTO.class)
                .setParameter("login", login)
                .setMaxResults(1).getResultList();
        if (list.isEmpty())
            return null;
        else
            return list.get(0);
    }

    @Nullable
    public UserDTO findUserByEmail(@Nullable String email) {
        List<UserDTO> list = entityManager
                .createQuery("SELECT e FROM SessionDTO e WHERE e.email = :email", UserDTO.class)
                .setParameter("email", email)
                .setMaxResults(1).getResultList();
        if (list.isEmpty())
            return null;
        else
            return list.get(0);
    }

    public void removeUserById(@Nullable String id) {
        UserDTO reference = entityManager.getReference(UserDTO.class, id);
        entityManager.remove(reference);
    }

    public void clear() {
        entityManager.createQuery("DELETE FROM UserDTO e").executeUpdate();
    }

    public int size() {
        return entityManager
                .createQuery("SELECT COUNT(e) FROM SessionDTO e", Integer.class)
                .getSingleResult();
    }
}
