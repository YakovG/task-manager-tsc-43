package ru.goloshchapov.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.goloshchapov.tm.api.repository.dto.ITaskRepositoryDTO;
import ru.goloshchapov.tm.dto.ProjectDTO;
import ru.goloshchapov.tm.dto.TaskDTO;

import javax.persistence.EntityManager;
import java.util.List;

public class TaskRepositoryDTO extends AbstractRepositoryDTO<TaskDTO> implements ITaskRepositoryDTO {

    public TaskRepositoryDTO(@NotNull EntityManager entityManager) {
        super(entityManager);
    }

    @Nullable
    public List<TaskDTO> findAllTasks() {
        return entityManager.createQuery( "SELECT e FROM TaskDTO e", TaskDTO.class).getResultList();
    }

    @Nullable
    public List<TaskDTO> findAllByUserId(@NotNull final String userId) {
        return entityManager
                .createQuery( "SELECT e FROM TaskDTO e WHERE e.userId = :userId", TaskDTO.class)
                .setParameter("userId", userId)
                .getResultList();
    }

    @Nullable
    public List<TaskDTO> findAll(@NotNull String userId) {
        return findAllByUserId(userId);
    }

    @Nullable
    public TaskDTO findTaskById(@NotNull final String id) {
        return entityManager.find(TaskDTO.class, id);
    }

    @Nullable
    public TaskDTO findOneById(
            @NotNull final String userId,
            @NotNull final String id
    ) {
        List<TaskDTO> list = entityManager
                .createQuery( "SELECT e FROM TaskDTO e WHERE e.userId = :userId AND e.id = :id", TaskDTO.class)
                .setParameter("userId", userId)
                .setParameter("id", id)
                .getResultList();
        if (list.isEmpty())
            return null;
        else
            return list.get(0);
    }

    @Nullable
    public TaskDTO findTaskByName(@NotNull final String name) {
        List<TaskDTO> list = entityManager
                .createQuery( "SELECT e FROM TaskDTO e WHERE e.name = :name", TaskDTO.class)
                .setParameter("name", name)
                .getResultList();
        if (list.isEmpty())
            return null;
        else
            return list.get(0);
    }

    @Nullable
    public TaskDTO findOneByName(
            @NotNull final String userId,
            @NotNull final String name
    ) {
        List<TaskDTO> list = entityManager
                .createQuery
                        ( "SELECT e FROM TaskDTO e WHERE e.userId = :userId AND e.name = :name", TaskDTO.class)
                .setParameter("name", name)
                .setParameter("userId", userId)
                .getResultList();
        if (list.isEmpty())
            return null;
        else
            return list.get(0);
    }

    public void removeOneById(@NotNull final String userId, @NotNull final String projectId) {
        remove(findOneById(userId, projectId));
    }

    public void removeTaskById(@NotNull final String id) {
        TaskDTO reference = entityManager.getReference(TaskDTO.class, id);
        entityManager.remove(reference);
    }

    public void clearAll() {
        entityManager.createQuery("DELETE FROM TaskDTO e").executeUpdate();
    }

    public void clear(@NotNull final String userId) {
        entityManager
                .createQuery("DELETE FROM TaskDTO e WHERE e.userId = :userId")
                .setParameter("userId", userId)
                .executeUpdate();
    }

    public int size() {
        return entityManager
                .createQuery("SELECT COUNT(e) FROM TaskDTO e", Integer.class)
                .getSingleResult();
    }

    @Nullable
    public List<TaskDTO> findAllByProjectId(
            @Nullable final String userId,
            @Nullable final String projectId
    ) {
        return entityManager
                .createQuery(
                        "SELECT e FROM TaskDTO e WHERE e.userId = :userId AND e.projectId = :projectId", TaskDTO.class)
                .setParameter("userId", userId)
                .setParameter("projectId", projectId)
                .getResultList();
    }

    
}
