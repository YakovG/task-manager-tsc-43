package ru.goloshchapov.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.goloshchapov.tm.api.repository.dto.IAbstractRepositoryDTO;
import ru.goloshchapov.tm.dto.AbstractEntityDTO;

import javax.persistence.EntityManager;
import java.util.Collection;

public class AbstractRepositoryDTO<E extends AbstractEntityDTO> implements IAbstractRepositoryDTO<E> {

    @NotNull
    protected final EntityManager entityManager;

    public AbstractRepositoryDTO(@NotNull final EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @Override
    public void add(@Nullable final E entity) {
        if (entity == null) return;
        entityManager.persist(entity);
    }

    @Override
    public void addAll(@Nullable final Collection<E> collection) {
        if (collection == null) return;
        for (E entity : collection) add(entity);
    }

    @Override
    public void update(@Nullable final E entity) {
        if (entity == null) return;
        entityManager.merge(entity);
    }

    @Override
    public void remove(@Nullable final E entity) {
        if (entity == null) return;
        entityManager.remove(entity);
    }



}
