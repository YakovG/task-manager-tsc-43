package ru.goloshchapov.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.goloshchapov.tm.api.repository.dto.IProjectRepositoryDTO;
import ru.goloshchapov.tm.dto.ProjectDTO;
import ru.goloshchapov.tm.dto.UserDTO;

import javax.persistence.EntityManager;
import java.util.List;

public class ProjectRepositoryDTO extends AbstractRepositoryDTO<ProjectDTO> implements IProjectRepositoryDTO {

    public ProjectRepositoryDTO(@NotNull EntityManager entityManager) {
        super(entityManager);
    }

    @Nullable
    public ProjectDTO findProjectById(@NotNull final String id) {
        return entityManager.find(ProjectDTO.class, id);
    }

    @Nullable
    public List<ProjectDTO> findAllProjects() {
        return entityManager.createQuery( "SELECT e FROM ProjectDTO e", ProjectDTO.class).getResultList();
    }

    @Nullable
    public List<ProjectDTO> findAllByUserId(@NotNull final String userId) {
        return entityManager
                .createQuery( "SELECT e FROM ProjectDTO e WHERE e.userId = :userId", ProjectDTO.class)
                .setParameter("userId", userId)
                .getResultList();
    }

    @Nullable
    public List<ProjectDTO> findAll(@NotNull String userId) {
        return findAllByUserId(userId);
    }

    @Nullable
    public ProjectDTO findOneById(
            @NotNull final String userId,
            @NotNull final String id
    ) {
        List<ProjectDTO> list = entityManager
                .createQuery( "SELECT e FROM ProjectDTO e WHERE e.userId = :userId AND e.id = :id", ProjectDTO.class)
                .setParameter("userId", userId)
                .setParameter("id", id)
                .getResultList();
        if (list.isEmpty())
            return null;
        else
            return list.get(0);
    }

    @Nullable
    public ProjectDTO findProjectByName(@NotNull final String name) {
        List<ProjectDTO> list = entityManager
                .createQuery( "SELECT e FROM ProjectDTO e WHERE e.name = :name", ProjectDTO.class)
                .setParameter("name", name)
                .getResultList();
        if (list.isEmpty())
            return null;
        else
            return list.get(0);
    }

    @Nullable
    public ProjectDTO findOneByName(
            @NotNull final String userId,
            @NotNull final String name
    ) {
        List<ProjectDTO> list = entityManager
                .createQuery
                        ( "SELECT e FROM ProjectDTO e WHERE e.userId = :userId AND e.name = :name", ProjectDTO.class)
                .setParameter("name", name)
                .setParameter("userId", userId)
                .getResultList();
        if (list.isEmpty())
            return null;
        else
            return list.get(0);
    }

    public void removeOneById(@NotNull final String userId, @NotNull final String projectId) {
        remove(findOneById(userId, projectId));
    }

    public void removeProjectById(@NotNull final String id) {
        ProjectDTO reference = entityManager.getReference(ProjectDTO.class, id);
        entityManager.remove(reference);
    }

    public void clearAll() {
        entityManager.createQuery("DELETE FROM ProjectDTO e").executeUpdate();
    }

    public void clear(@NotNull final String userId) {
        entityManager
                .createQuery("DELETE FROM ProjectDTO e WHERE e.userId = :userId")
                .setParameter("userId", userId)
                .executeUpdate();
    }

    public int size() {
        return entityManager
                .createQuery("SELECT COUNT(e) FROM ProjectDTO e", Integer.class)
                .getSingleResult();
    }

}
