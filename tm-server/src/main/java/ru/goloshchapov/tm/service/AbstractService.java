package ru.goloshchapov.tm.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.goloshchapov.tm.api.IService;
import ru.goloshchapov.tm.api.service.ServiceLocator;
import ru.goloshchapov.tm.exception.entity.ElementsNotFoundException;
import ru.goloshchapov.tm.exception.system.IndexIncorrectException;
import ru.goloshchapov.tm.dto.AbstractEntityDTO;

import java.util.Collection;
import java.util.List;

import static ru.goloshchapov.tm.util.ValidationUtil.*;

public abstract class AbstractService<E extends AbstractEntityDTO> implements IService<E> {

    @NotNull
    private final ServiceLocator serviceLocator;

    public AbstractService(@NotNull final ServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @Override
    @SneakyThrows
    public void addAll(@Nullable final Collection<E> collection) {
        if (collection == null || collection.isEmpty()) return;
        for (E entity: collection) {
            add(entity);
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public E findOneByIndex(@Nullable final Integer index) {
        if (index == null) throw new IndexIncorrectException();
        if (!checkIndex(index,size())) throw new IndexIncorrectException();
        @Nullable final List<E> entities = findAll();
        if (entities == null) throw new ElementsNotFoundException();
        @NotNull final E entity = entities.get(index);
        return null;
    }

    @Override
    @SneakyThrows
    public boolean isAbsentById(@Nullable final String id) {
        if (isEmpty(id)) return true;
        return findOneById(id) == null;
    }

    @Override
    @SneakyThrows
    public boolean isAbsentByIndex(@Nullable final Integer index) {
        if (index == null) throw new IndexIncorrectException();
        if (!checkIndex(index, size())) return true;
        return findOneByIndex(index) == null;
    }

    @Nullable
    @Override
    @SneakyThrows
    public String getIdByIndex(@Nullable final Integer index) {
        if (index == null) throw new IndexIncorrectException();
        if (!checkIndex(index, size())) throw new IndexIncorrectException();
        return findOneByIndex(index).getId();
    }

    @Override
    @SneakyThrows
    public int size() {
        @Nullable final List<E> entities = findAll();
        if (entities == null) throw new ElementsNotFoundException();
        return entities.size();
    }

    @Override
    @SneakyThrows
    public void clear() {
        @Nullable final List<E> entities = findAll();
        if (entities == null) throw new ElementsNotFoundException();
        for (E entity: entities) {
            remove(entity);
        }
    }

    @Override
    @SneakyThrows
    public void remove(@Nullable final E entity) {
        if (entity == null) return;
        removeOneById(entity.getId());
    }

    @Nullable
    @Override
    @SneakyThrows
    public E removeOneByIndex(@Nullable final Integer index) {
        if (index == null) throw new IndexIncorrectException();
        if (!checkIndex(index,size())) throw new IndexIncorrectException();
        @Nullable final E entity = findOneByIndex(index);
        if (entity == null) throw new ElementsNotFoundException();
        return removeOneById(entity.getId());
    }
}
