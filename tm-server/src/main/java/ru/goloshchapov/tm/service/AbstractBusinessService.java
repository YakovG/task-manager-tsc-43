package ru.goloshchapov.tm.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.goloshchapov.tm.api.IBusinessService;
import ru.goloshchapov.tm.api.service.ServiceLocator;
import ru.goloshchapov.tm.constant.SortConst;
import ru.goloshchapov.tm.enumerated.Sort;
import ru.goloshchapov.tm.enumerated.Status;
import ru.goloshchapov.tm.exception.auth.AccessDeniedException;
import ru.goloshchapov.tm.exception.empty.EmptyIdException;
import ru.goloshchapov.tm.exception.empty.EmptyNameException;
import ru.goloshchapov.tm.exception.entity.ElementsNotFoundException;
import ru.goloshchapov.tm.exception.entity.RemoveImpossibleException;
import ru.goloshchapov.tm.exception.incorrect.StatusIncorrectException;
import ru.goloshchapov.tm.exception.system.IndexIncorrectException;
import ru.goloshchapov.tm.dto.AbstractBusinessEntityDTO;

import java.util.*;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import static ru.goloshchapov.tm.util.ValidationUtil.*;

public abstract class AbstractBusinessService<M extends AbstractBusinessEntityDTO>
        extends AbstractService<M> implements IBusinessService<M> {

    @NotNull
    private final ServiceLocator serviceLocator;

    @NotNull
    public AbstractBusinessService(@NotNull ServiceLocator serviceLocator) {
        super(serviceLocator);
        this.serviceLocator = serviceLocator;
    }

    public Predicate<M> predicateByStarted(@NotNull final String userId) {
        return m -> m.checkUserAccess(userId) && (m.getStatus() != Status.NOT_STARTED);
    }

    public Predicate<M> predicateByCompleted(@NotNull final String userId) {
        return m -> m.checkUserAccess(userId) && (m.getStatus() == Status.COMPLETE);
    }

    private void checkDateByStatus(@NotNull final M model) {
        @NotNull final Status status = model.getStatus();
        @Nullable final Date dateStart = model.getDateStart();
        @Nullable final Date dateFinish = model.getDateFinish();
        @NotNull final Date dateNow = new Date();
        switch (status) {
            case COMPLETE:
                if (dateStart == null) model.setDateStart(dateNow);
                if (dateFinish == null) model.setDateFinish(dateNow);
                break;
            case IN_PROGRESS:
                if (dateStart == null) model.setDateStart(new Date());
                model.setDateFinish(null);
                break;
            default:
                model.setDateStart(null);
                model.setDateFinish(null);
        }
    }

    public abstract List<M> findAll(@Nullable final String userId);

    public abstract M findOneById(@Nullable final String userId, @Nullable final String modelId);

    public abstract M findOneByName(@Nullable final String userId, @Nullable final String name);

    public abstract M findOneByName(@Nullable final String name);

    public abstract M removeOneById(@Nullable final String userId, @Nullable final String modelId);

    public abstract M update(@Nullable M model);

    @NotNull
    private M start (@NotNull final M m) {
        m.setStatus(Status.IN_PROGRESS);
        m.setDateStart(new Date());
        update(m);
        return m;
    }

    @NotNull
    private M finish (@NotNull final M m) {
        m.setStatus(Status.COMPLETE);
        m.setDateFinish(new Date());
        update(m);
        return m;
    }

    @Override
    @SneakyThrows
    public void addAll(@Nullable final String userId, @Nullable final Collection<M> collection) {
        if (isEmpty(userId)) throw new AccessDeniedException();
        if (collection == null || collection.isEmpty()) return;
        collection.forEach(c->c.setUserId(userId));
        for (M model: collection) {
            add(model);
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public List<M> findAll(@NotNull final String userId, @Nullable final Comparator<M> comparator) {
        if (isEmpty(userId)) throw new AccessDeniedException();
        if (comparator == null) return null;
        @Nullable final List<M> models = findAll(userId);
        if (models == null) throw new ElementsNotFoundException();
        return models.stream()
                .sorted(comparator)
                .collect(Collectors.toList());
    }

    @Nullable
    @SneakyThrows
    public List<M> findAllStarted(@Nullable final String userId, @Nullable final Comparator<M> comparator) {
        if (isEmpty(userId)) throw new AccessDeniedException();
        if (comparator == null) return null;
        @Nullable final List<M> models = findAll(userId);
        if (models == null) throw new ElementsNotFoundException();
        @Nullable final List<M> result = models.stream()
                                    .filter(predicateByStarted(userId))
                                    .sorted(comparator)
                                    .collect(Collectors.toList());
        if (result.isEmpty()) throw new ElementsNotFoundException();
        return result;
    }


    @Nullable
    @SneakyThrows
    public List<M> findAllCompleted(@Nullable final String userId, @Nullable final Comparator<M> comparator) {
        if (isEmpty(userId)) throw new AccessDeniedException();
        if (comparator == null) return null;
        final List<M> models = findAll(userId);
        if (models == null) throw new ElementsNotFoundException();
        @Nullable final List<M> result = models.stream()
                                        .filter(predicateByCompleted(userId))
                                        .sorted(comparator)
                                        .collect(Collectors.toList());
        if (result.isEmpty()) throw new ElementsNotFoundException();
        return result;
    }

    @Nullable
    @Override
    @SneakyThrows
    public List<M> sortedBy(@Nullable final String userId, @Nullable final String sortCheck) {
        if (isEmpty(userId)) throw new AccessDeniedException();
        @Nullable final Sort[] sortOptions = Sort.values();
        @NotNull String sortChoice = SortConst.STATUS_DEFAULT;
        if (!isEmpty(sortCheck) && checkInclude(sortCheck,toStringArray(sortOptions))) sortChoice = sortCheck;
        @NotNull final Sort sortType = Sort.valueOf(sortChoice);
        switch (sortType) {
            case DATE_START: return findAllStarted(userId, sortType.getComparator());
            case DATE_FINISH: return findAllCompleted(userId, sortType.getComparator());
            default: return findAll(userId, sortType.getComparator());
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public M findOneByIndex(@Nullable final String userId, @Nullable final Integer index) {
        if (isEmpty(userId)) throw new AccessDeniedException();
        @Nullable final List<M> models = findAll(userId);
        if (models == null) throw new ElementsNotFoundException();
        final int checkIndex = size();
        if (index == null) throw new IndexIncorrectException();
        if (checkIndex > index) throw new IndexIncorrectException();
        @NotNull final M model = models.get(index);
        if (model.checkUserAccess(userId)) return model;
        return null;

    }

    @SneakyThrows
    public boolean isAbsentByName(@Nullable final String name) {
        if (isEmpty(name)) return true;
        return findOneByName(name) == null;
    }

    @Override
    @SneakyThrows
    public boolean isAbsentById(@NotNull String userId, @NotNull String projectId) {
        if (isEmpty(userId)) throw new EmptyIdException();
        if (isEmpty(projectId)) throw new EmptyIdException();
        return findOneById(userId, projectId) == null;
    }

    @Override
    @SneakyThrows
    public boolean isAbsentByName(@NotNull String userId, @NotNull String projectName) {
        if (isEmpty(userId)) throw new EmptyIdException();
        if (isEmpty(projectName)) throw new EmptyNameException();
        return findOneById(userId, projectName) == null;
    }

    @Override
    @Nullable
    @SneakyThrows
    public String getIdByName(@Nullable final String name) {
        if (isEmpty(name)) throw new EmptyNameException();
        @Nullable final M model = findOneByName(name);
        if (model == null) return null;
        return model.getId();
    }

    @Override
    @SneakyThrows
    public int size(@Nullable final String userId) {
        if (isEmpty(userId)) throw new AccessDeniedException();
        @Nullable final List<M> models = findAll(userId);
        if (models == null) return 0;
        return models.size();
    }

    @Override
    @SneakyThrows
    public void remove(@Nullable final String userId, @Nullable final M model) {
        if (isEmpty(userId)) throw new AccessDeniedException();
        if (model == null) throw new RemoveImpossibleException();
        if (!model.checkUserAccess(userId)) throw new AccessDeniedException();
        removeOneById(userId, model.getId());
    }

    @Nullable
    @Override
    @SneakyThrows
    public M removeOneByIndex(@Nullable final String userId, @Nullable final Integer index) {
        if (isEmpty(userId)) throw new AccessDeniedException();
        final M model = findOneByIndex(userId, index);
        if (model == null) return null;
        return removeOneById(model.getId());
    }

    @Override
    @Nullable
    @SneakyThrows
    public M removeOneByName(@Nullable final String userId, @Nullable final String name) {
        if (isEmpty(userId)) throw new AccessDeniedException();
        if (isEmpty(name)) throw new EmptyNameException();
        @Nullable final String id = getIdByName(name);
        if (isEmpty(id)) throw new ElementsNotFoundException();
        return removeOneById(userId, id);
    }

    @Nullable
    @Override
    @SneakyThrows
    public M updateOneById(
            @Nullable final String userId, @Nullable final String modelId,
            @Nullable final String name, @Nullable final String description
    ) {
        if (isEmpty(userId)) throw new AccessDeniedException();
        if (isEmpty(modelId)) throw new EmptyIdException();
        if (isEmpty(name)) throw new EmptyNameException();
        if (isAbsentById(modelId)) return null;
        @Nullable final M model = findOneById(userId, modelId);
        if (model == null) return null;
        model.setId(modelId);
        model.setName(name);
        model.setDescription(description);
        @Nullable final M result = update(model);
        return result;
    }

    @Nullable
    @Override
    @SneakyThrows
    public M updateOneByIndex(
            @Nullable final String userId, @Nullable final Integer index,
            @Nullable final String name, @Nullable final String description
    ) {
        if (isEmpty(userId)) throw new AccessDeniedException();
        final int size = findAll().size();
        if (!checkIndex(index,size)) throw new IndexIncorrectException();
        if (isEmpty(name)) throw new EmptyNameException();
        if (isAbsentByIndex(index)) return null;
        @Nullable final M model = findOneByIndex(userId, index);
        if (model == null) return null;
        model.setName(name);
        model.setDescription(description);
        @Nullable final M result = update(model);
        return result;
    }

    @Override
    @Nullable
    @SneakyThrows
    public M startOneById(@Nullable final String userId, @Nullable final String modelId) {
        if (isEmpty(userId)) throw new AccessDeniedException();
        if (isEmpty(modelId)) throw new EmptyIdException();
        @Nullable final M model = findOneById(userId, modelId);
        if (model == null) return null;
        @NotNull final M result = start(model);
        checkDateByStatus(result);
        return update(result);
    }

    @Override
    @Nullable
    @SneakyThrows
    public M startOneByIndex(@Nullable final String userId, @Nullable final Integer index) {
        if (isEmpty(userId)) throw new AccessDeniedException();
        if (index == null) throw new IndexIncorrectException();
        final int size = size();
        if (!checkIndex(index, size)) throw new IndexIncorrectException();
        @Nullable final M model = findOneByIndex(userId, index);
        if (model == null) return null;
        @NotNull final M result = start(model);
        checkDateByStatus(result);
        return update(result);
    }

    @Override
    @Nullable
    @SneakyThrows
    public M startOneByName(@Nullable final String userId, @Nullable final String name) {
        if (isEmpty(userId)) throw new AccessDeniedException();
        if (isEmpty(name)) throw new EmptyNameException();
        @Nullable final M model = findOneByName(userId, name);
        if (model == null) return null;
        @NotNull final M result = start(model);
        checkDateByStatus(model);
        return update(result);
    }

    @Override
    @Nullable
    @SneakyThrows
    public M finishOneById(@Nullable final String userId, @Nullable final String modelId) {
        if (isEmpty(userId)) throw new AccessDeniedException();
        if (isEmpty(modelId)) throw new EmptyIdException();
        @Nullable final M model = findOneById(userId, modelId);
        if (model == null) return null;
        @NotNull final M result = finish(model);
        checkDateByStatus(model);
        return update(result);
    }

    @Override
    @Nullable
    @SneakyThrows
    public M finishOneByIndex(@Nullable final String userId, @Nullable final Integer index) {
        if (isEmpty(userId)) throw new AccessDeniedException();
        if (index == null) throw new IndexIncorrectException();
        final int size = size();
        if (!checkIndex(index, size)) throw new IndexIncorrectException();
        @Nullable final M model = findOneByIndex(userId, index);
        if (model == null) return null;
        @NotNull final M result = finish(model);
        checkDateByStatus(model);
        return update(result);
    }

    @Override
    @Nullable
    @SneakyThrows
    public M finishOneByName(@Nullable final String userId, @Nullable final String name) {
        if (isEmpty(userId)) throw new AccessDeniedException();
        if (isEmpty(name)) throw new EmptyNameException();
        @Nullable final M model = findOneByName(userId, name);
        if (model == null) return null;
        @NotNull final M result = finish(model);
        checkDateByStatus(model);
        return update(result);
    }

    @Nullable
    @Override
    @SneakyThrows
    public M changeOneStatusById(
            @Nullable final String userId, @Nullable final String modelId, @Nullable final String statusChange
    ) {
        if (isEmpty(userId)) throw new AccessDeniedException();
        if (isEmpty(modelId)) throw new EmptyIdException();
        if (isAbsentById(modelId)) return null;
        @Nullable Status[] statuses = Status.values();
        if (!checkInclude(statusChange, toStringArray(statuses))) throw new StatusIncorrectException(statusChange);
        @NotNull final Status status = Status.valueOf(statusChange);
        @Nullable final M model = findOneById(userId, modelId);
        if (model == null) return null;
        model.setStatus(status);
        checkDateByStatus(model);
        @Nullable final M result = update(model);
        return result;
    }

    @Nullable
    @Override
    @SneakyThrows
    public M changeOneStatusByName(
            @Nullable final String userId, @Nullable final String name, @Nullable final String statusChange
    ) {
        if (isEmpty(userId)) throw new AccessDeniedException();
        if (isEmpty(name)) throw new EmptyNameException();
        if (isAbsentByName(name)) return null;
        @Nullable Status[] statuses = Status.values();
        if (!checkInclude(statusChange, toStringArray(statuses))) throw new StatusIncorrectException(statusChange);
        @NotNull final Status status = Status.valueOf(statusChange);
        @Nullable final M model = findOneByName(userId, name);
        if (model == null) return null;
        model.setStatus(status);
        checkDateByStatus(model);
        @Nullable final M result = update(model);
        return result;
    }

    @Nullable
    @Override
    @SneakyThrows
    public M changeOneStatusByIndex(
            @Nullable final String userId, final int index, @Nullable final String statusChange
    ) {
        if (isEmpty(userId)) throw new AccessDeniedException();
        final int size = size();
        if (!checkIndex(index, size)) throw new IndexIncorrectException();
        if (isAbsentByIndex(index)) return null;
        @Nullable Status[] statuses = Status.values();
        if (!checkInclude(statusChange, toStringArray(statuses))) throw new StatusIncorrectException(statusChange);
        @NotNull final Status status = Status.valueOf(statusChange);
        @Nullable final M model = findOneByIndex(userId, index);
        if (model == null) return null;
        model.setStatus(status);
        checkDateByStatus(model);
        @Nullable final M result = update(model);
        return result;
    }
}
