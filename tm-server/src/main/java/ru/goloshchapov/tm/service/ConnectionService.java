package ru.goloshchapov.tm.service;

import org.apache.ibatis.datasource.pooled.PooledDataSource;
import org.apache.ibatis.mapping.Environment;
import org.apache.ibatis.session.Configuration;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.apache.ibatis.transaction.TransactionFactory;
import org.apache.ibatis.transaction.jdbc.JdbcTransactionFactory;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.jetbrains.annotations.NotNull;
import ru.goloshchapov.tm.api.IPropertyService;
import ru.goloshchapov.tm.api.repository.*;
import ru.goloshchapov.tm.api.service.IConnectionService;
import ru.goloshchapov.tm.dto.ProjectDTO;
import ru.goloshchapov.tm.dto.SessionDTO;
import ru.goloshchapov.tm.dto.TaskDTO;
import ru.goloshchapov.tm.dto.UserDTO;
import ru.goloshchapov.tm.model.Project;
import ru.goloshchapov.tm.model.Session;
import ru.goloshchapov.tm.model.Task;
import ru.goloshchapov.tm.model.User;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;
import java.util.HashMap;
import java.util.Map;

public final class ConnectionService implements IConnectionService {

    private final IPropertyService propertyService;

    private final SqlSessionFactory sqlSessionFactory;

    private final EntityManagerFactory entityManagerFactory;

    public ConnectionService(IPropertyService propertyService) {
        this.propertyService = propertyService;
        sqlSessionFactory = getSqlSessionFactory();
        entityManagerFactory = factory();
    }

    @Override
    @NotNull
    public SqlSession getSqlSession() {
        return sqlSessionFactory.openSession();
    }

    @Override
    public SqlSessionFactory getSqlSessionFactory() {
        @NotNull final String url = propertyService.getJdbcUrl();
        @NotNull final String driver = propertyService.getJdbcDriver();
        @NotNull final String user = propertyService.getJdbcUsername();
        @NotNull final String password = propertyService.getJdbcPassword();
        @NotNull final DataSource dataSource = new PooledDataSource(driver, url, user, password);
        @NotNull final TransactionFactory transactionFactory = new JdbcTransactionFactory();
        @NotNull final Environment environment = new Environment("development", transactionFactory, dataSource);
        @NotNull final Configuration configuration = new Configuration(environment);
        configuration.addMapper(IProjectRepository.class);
        configuration.addMapper(ITaskRepository.class);
        configuration.addMapper(IUserRepository.class);
        configuration.addMapper(ISessionRepository.class);
        configuration.addMapper(ITestRepository.class);
        return new SqlSessionFactoryBuilder().build(configuration);
    }

    private EntityManagerFactory factory() {
        final Map<String, String> settings = new HashMap<>();
        settings.put(org.hibernate.cfg.Environment.DRIVER, propertyService.getJdbcDriver());
        settings.put(org.hibernate.cfg.Environment.URL, propertyService.getJdbcUrl());
        settings.put(org.hibernate.cfg.Environment.USER, propertyService.getJdbcUsername());
        settings.put(org.hibernate.cfg.Environment.PASS, propertyService.getJdbcPassword());
        settings.put(org.hibernate.cfg.Environment.DIALECT, propertyService.getHibernateDialect());
        settings.put(org.hibernate.cfg.Environment.HBM2DDL_AUTO, propertyService.getHbm2ddlAuto());
        settings.put(org.hibernate.cfg.Environment.SHOW_SQL, propertyService.getShowSql());

        final StandardServiceRegistryBuilder registryBuilder =
                new StandardServiceRegistryBuilder();
        registryBuilder.applySettings(settings);
        final StandardServiceRegistry registry = registryBuilder.build();
        final MetadataSources sources = new MetadataSources(registry);

        sources.addAnnotatedClass(ProjectDTO.class);
        sources.addAnnotatedClass(Project.class);

        sources.addAnnotatedClass(TaskDTO.class);
        sources.addAnnotatedClass(Task.class);

        sources.addAnnotatedClass(SessionDTO.class);
        sources.addAnnotatedClass(Session.class);

        sources.addAnnotatedClass(UserDTO.class);
        sources.addAnnotatedClass(User.class);

        final Metadata metadata = sources.getMetadataBuilder().build();
        return metadata.getSessionFactoryBuilder().build();
    }
}
