package ru.goloshchapov.tm.service;

import lombok.SneakyThrows;
import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.goloshchapov.tm.api.repository.IProjectRepository;
import ru.goloshchapov.tm.api.service.IProjectService;
import ru.goloshchapov.tm.api.service.ServiceLocator;
import ru.goloshchapov.tm.exception.auth.AccessDeniedException;
import ru.goloshchapov.tm.exception.empty.EmptyIdException;
import ru.goloshchapov.tm.exception.empty.EmptyNameException;
import ru.goloshchapov.tm.exception.entity.ElementsNotFoundException;
import ru.goloshchapov.tm.dto.ProjectDTO;

import java.util.List;

import static ru.goloshchapov.tm.util.ValidationUtil.*;

public final class ProjectService extends AbstractBusinessService<ProjectDTO> implements IProjectService {

    @NotNull private final ServiceLocator serviceLocator;

    @NotNull
    public ProjectService(@NotNull final ServiceLocator serviceLocator) {
        super(serviceLocator);
        this.serviceLocator = serviceLocator;
    }

    @Nullable
    @Override
    @SneakyThrows
    public ProjectDTO add(@Nullable final ProjectDTO project) {
        if (project == null) throw new ElementsNotFoundException();
        @NotNull final SqlSession sqlSession = serviceLocator.getConnectionService().getSqlSession();
        try {
            @NotNull final IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);
            projectRepository.add(project);
            sqlSession.commit();
            return project;
        } catch (final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Override
    @Nullable
    @SneakyThrows
    public ProjectDTO add(@Nullable final String userId, @Nullable final ProjectDTO project) {
        if (project == null) throw new ElementsNotFoundException();
        if (isEmpty(userId)) throw new EmptyIdException();
        project.setUserId(userId);
        return add(project);
    }

    @Nullable
    @Override
    @SneakyThrows
    public ProjectDTO add(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (isEmpty(userId)) throw new AccessDeniedException();
        if (isEmpty(name)) throw new EmptyNameException();
        @NotNull final ProjectDTO project = new ProjectDTO();
        project.setName(name);
        project.setDescription(description);
        @NotNull final SqlSession sqlSession = serviceLocator.getConnectionService().getSqlSession();
        try {
            @NotNull final IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);
            project.setUserId(userId);
            projectRepository.add(project);
            sqlSession.commit();
            return project;
        } catch (final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Override
    @Nullable
    @SneakyThrows
    public ProjectDTO update(@Nullable ProjectDTO project) {
        if (project == null) return null;
        @NotNull final SqlSession sqlSession = serviceLocator.getConnectionService().getSqlSession();
        try {
            @NotNull final IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);
            projectRepository.update(project);
            sqlSession.commit();
            return project;
        } catch (final Exception e) {
            sqlSession.rollback();
            throw  e;
        } finally {
            sqlSession.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public List<ProjectDTO> findAll() {
        @NotNull final SqlSession sqlSession = serviceLocator.getConnectionService().getSqlSession();
        try {
            @NotNull final IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);
            @Nullable final List<ProjectDTO> projects = projectRepository.findAllProjects();
            if (projects == null) throw new ElementsNotFoundException();
            return projects;
        } catch (final Exception e) {
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public List<ProjectDTO> findAll(@Nullable final String userId) {
        if (isEmpty(userId)) throw new AccessDeniedException();
        @NotNull final SqlSession sqlSession = serviceLocator.getConnectionService().getSqlSession();
        try {
            @NotNull final IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);
            @Nullable final List<ProjectDTO> projects = projectRepository.findAll(userId);
            if (projects == null) throw new ElementsNotFoundException();
            return projects;
        } catch (final Exception e) {
            throw e;
        } finally {
            sqlSession.close();
        }
    }


    @Nullable
    @Override
    @SneakyThrows
    public List<ProjectDTO> findAllByUserId(@Nullable final String userId) {
        if (isEmpty(userId)) throw new AccessDeniedException();
        @NotNull final SqlSession sqlSession = serviceLocator.getConnectionService().getSqlSession();
        try {
            @NotNull final IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);
            @Nullable final List<ProjectDTO> projects = projectRepository.findAllByUserId(userId);
            if (projects == null) throw new ElementsNotFoundException();
            return projects;
        } catch (final Exception e) {
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public ProjectDTO findOneById(@Nullable final String userId, @Nullable final String projectId) {
        if (isEmpty(userId)) throw new AccessDeniedException();
        if (isEmpty(projectId)) throw new EmptyIdException();
        @NotNull final SqlSession sqlSession = serviceLocator.getConnectionService().getSqlSession();
        try {
            @NotNull final IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);
            return projectRepository.findOneById(userId, projectId);
        } finally {
            sqlSession.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public ProjectDTO findOneById(@Nullable final String projectId) {
        if (isEmpty(projectId)) throw new EmptyIdException();
        @NotNull final SqlSession sqlSession = serviceLocator.getConnectionService().getSqlSession();
        try {
            @NotNull final IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);
            return projectRepository.findProjectById(projectId);
        } finally {
            sqlSession.close();
        }
    }

    @Override
    @Nullable
    @SneakyThrows
    public ProjectDTO findOneByName(@Nullable final String name) {
        if (isEmpty(name)) throw new EmptyNameException();
        @NotNull final SqlSession sqlSession = serviceLocator.getConnectionService().getSqlSession();
        try {
            @NotNull final IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);
            return projectRepository.findProjectByName(name);
        } finally {
            sqlSession.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public ProjectDTO findOneByName(@Nullable final String userId, @Nullable final String name) {
        if (isEmpty(userId)) throw new AccessDeniedException();
        if (isEmpty(name)) throw new EmptyNameException();
        @NotNull final SqlSession sqlSession = serviceLocator.getConnectionService().getSqlSession();
        try {
            @NotNull final IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);
            return projectRepository.findOneByName(userId, name);
        } finally {
            sqlSession.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public ProjectDTO removeOneById(@Nullable final String userId, @Nullable final String projectId) {
        if (isEmpty(userId)) throw new AccessDeniedException();
        if (isEmpty(projectId)) throw new EmptyIdException();
        @Nullable final ProjectDTO result = findOneById(userId, projectId);
        if (result == null) throw new ElementsNotFoundException();
        @NotNull final SqlSession sqlSession = serviceLocator.getConnectionService().getSqlSession();
        try {
            @NotNull final IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);
            projectRepository.removeOneById(userId, projectId);
            sqlSession.commit();
            return result;
        } catch (final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public ProjectDTO removeOneById(@Nullable final String projectId) {
        if (isEmpty(projectId)) throw new EmptyIdException();
        @Nullable final ProjectDTO result = findOneById(projectId);
        if (result == null) throw new ElementsNotFoundException();
        @NotNull final SqlSession sqlSession = serviceLocator.getConnectionService().getSqlSession();
        try {
            @NotNull final IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);
            projectRepository.removeProjectById(projectId);
            sqlSession.commit();
            return result;
        } catch (final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Override
    @SneakyThrows
    public void clear(@Nullable final String userId) {
        if (isEmpty(userId)) throw new AccessDeniedException();
        @NotNull final SqlSession sqlSession = serviceLocator.getConnectionService().getSqlSession();
        try {
            @NotNull final IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);
            projectRepository.clear(userId);
            sqlSession.commit();
        } catch (final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }
}

