package ru.goloshchapov.tm.endpoint;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.goloshchapov.tm.api.endpoint.ITaskEndpoint;
import ru.goloshchapov.tm.api.service.ServiceLocator;
import ru.goloshchapov.tm.dto.SessionDTO;
import ru.goloshchapov.tm.dto.TaskDTO;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.Collection;
import java.util.List;

@WebService
public final class TaskEndpoint extends AbstractEndpoint implements ITaskEndpoint {

    private final ServiceLocator serviceLocator;

    public TaskEndpoint (ServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @Override
    @WebMethod
    @SneakyThrows
    public void addTaskAll(
            @WebParam(name = "session") final SessionDTO session,
            @WebParam(name = "collection") Collection<TaskDTO> collection
    ) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getTaskService().addAll(session.getUserId(), collection);
    }

    @Override
    @WebMethod
    @SneakyThrows
    public @Nullable TaskDTO addTask(
            @WebParam(name = "session") final SessionDTO session,
            @WebParam(name = "task") final TaskDTO model
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getTaskService().add(session.getUserId(), model);
    }

    @Override
    @WebMethod
    @SneakyThrows
    public @NotNull TaskDTO addTaskByName(
            @WebParam(name = "session") final SessionDTO session,
            @WebParam(name = "name") final String name,
            @WebParam(name = "description") final String description
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getTaskService().add(session.getUserId(), name, description);
    }

    @Override
    @WebMethod
    @SneakyThrows
    public @Nullable List<TaskDTO> findTaskAll(
            @WebParam(name = "session") final SessionDTO session
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getTaskService().findAll(session.getUserId());
    }

    @Override
    @WebMethod
    @SneakyThrows
    public @Nullable List<TaskDTO> findTaskAllByUserId(
            @WebParam(name = "session") final SessionDTO session
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getTaskService().findAllByUserId(session.getUserId());
    }

    @Override
    @WebMethod
    @SneakyThrows
    public @Nullable TaskDTO findTaskById(
            @WebParam(name = "session") final SessionDTO session,
            @WebParam(name = "taskId") final String modelId
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getTaskService().findOneById(session.getUserId(), modelId);
    }

    @Override
    @WebMethod
    @SneakyThrows
    public @Nullable TaskDTO findTaskByIndex(
            @WebParam(name = "session") final SessionDTO session,
            @WebParam(name = "index") final Integer index
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getTaskService().findOneByIndex(session.getUserId(), index);
    }

    @Override
    @WebMethod
    @SneakyThrows
    public @Nullable TaskDTO findTaskByName(
            @WebParam(name = "session") final SessionDTO session,
            @WebParam(name = "name") final String name
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getTaskService().findOneByName(session.getUserId(), name);
    }


    @Override
    @WebMethod
    @SneakyThrows
    public int sizeTask(
            @WebParam(name = "session") final SessionDTO session
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getTaskService().size(session.getUserId());
    }

    @Override
    @WebMethod
    @SneakyThrows
    public void removeTask(
            @WebParam(name = "session") final SessionDTO session,
            @WebParam(name = "task") final TaskDTO model
    ) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getTaskService().remove(session.getUserId(), model);
    }

    @Override
    @WebMethod
    @SneakyThrows
    public void clearTask(
            @WebParam(name = "session") final SessionDTO session
    ) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getTaskService().clear(session.getUserId());
    }

    @Override
    @WebMethod
    @SneakyThrows
    public @Nullable TaskDTO removeTaskById(
            @WebParam(name = "session") final SessionDTO session,
            @WebParam(name = "taskId") final String modelId
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getTaskService().removeOneById(session.getUserId(), modelId);
    }

    @Override
    @WebMethod
    @SneakyThrows
    public @Nullable TaskDTO removeTaskByIndex(
            @WebParam(name = "session") final SessionDTO session,
            @WebParam(name = "index") final Integer index
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getTaskService().removeOneByIndex(session.getUserId(), index);
    }

    @Override
    @WebMethod
    @SneakyThrows
    public @Nullable TaskDTO removeTaskByName(
            @WebParam(name = "session") final SessionDTO session,
            @WebParam(name = "name") final String name
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getTaskService().removeOneByName(session.getUserId(), name);
    }

    @Override
    @WebMethod
    @SneakyThrows
    public @Nullable TaskDTO startTaskById(
            @WebParam(name = "session") final SessionDTO session,
            @WebParam(name = "taskId") final String modelId
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getTaskService().startOneById(session.getUserId(), modelId);
    }

    @Override
    @WebMethod
    @SneakyThrows
    public @Nullable TaskDTO startTaskByIndex(
            @WebParam(name = "session") final SessionDTO session,
            @WebParam(name = "index") final Integer index
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getTaskService().startOneByIndex(session.getUserId(), index);
    }

    @Override
    @WebMethod
    @SneakyThrows
    public @Nullable TaskDTO startTaskByName(
            @WebParam(name = "session") final SessionDTO session,
            @WebParam(name = "name") final String name
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getTaskService().startOneByName(session.getUserId(), name);
    }

    @Override
    @WebMethod
    @SneakyThrows
    public @Nullable TaskDTO finishTaskById(
            @WebParam(name = "session") final SessionDTO session,
            @WebParam(name = "taskId") final String modelId
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getTaskService().finishOneById(session.getUserId(), modelId);
    }

    @Override
    @WebMethod
    @SneakyThrows
    public @Nullable TaskDTO finishTaskByIndex(
            @WebParam(name = "session") final SessionDTO session,
            @WebParam(name = "index") final Integer index
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getTaskService().finishOneByIndex(session.getUserId(), index);
    }

    @Override
    @WebMethod
    @SneakyThrows
    public @Nullable TaskDTO finishTaskByName(
            @WebParam(name = "session") final SessionDTO session,
            @WebParam(name = "name") final String name
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getTaskService().finishOneByName(session.getUserId(), name);
    }

    @Override
    @WebMethod
    @SneakyThrows
    public @Nullable List<TaskDTO> sortedTaskBy(
            @WebParam(name = "session") final SessionDTO session,
            @WebParam(name = "sortCheck") final String sortCheck) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getTaskService().sortedBy(session.getUserId(), sortCheck);
    }

    @Override
    @WebMethod
    @SneakyThrows
    public @Nullable TaskDTO updateTaskById(
            @WebParam(name = "session") final SessionDTO session,
            @WebParam(name = "taskId") final String modelId,
            @WebParam(name = "name") final String name,
            @WebParam(name = "description") final String description
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getTaskService().updateOneById(session.getUserId(), modelId, name, description);
    }

    @Override
    @WebMethod
    @SneakyThrows
    public @Nullable TaskDTO updateTaskByIndex(
            @WebParam(name = "session") final SessionDTO session,
            @WebParam(name = "index") final Integer index,
            @WebParam(name = "name") String name,
            @WebParam(name = "description") String description
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getTaskService().updateOneByIndex(session.getUserId(), index, name, description);
    }

    @Override
    @WebMethod
    @SneakyThrows
    public @Nullable TaskDTO changeTaskStatusById(
            @WebParam(name = "session") final SessionDTO session,
            @WebParam(name = "taskId") final String id,
            @WebParam(name = "statusChange") String statusChange
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getTaskService().changeOneStatusById(session.getUserId(), id, statusChange);
    }

    @Override
    @WebMethod
    @SneakyThrows
    public @Nullable TaskDTO changeTaskStatusByName(
            @WebParam(name = "session") final SessionDTO session,
            @WebParam(name = "name") final String name,
            @WebParam(name = "statusChange") String statusChange
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getTaskService().changeOneStatusByName(session.getUserId(), name, statusChange);
    }

    @Override
    @WebMethod
    @SneakyThrows
    public @Nullable TaskDTO changeTaskStatusByIndex(
            @WebParam(name = "session") final SessionDTO session,
            @WebParam(name = "index") final int index,
            @WebParam(name = "statusChange") String statusChange
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getTaskService().changeOneStatusByIndex(session.getUserId(), index, statusChange);
    }

    @Override
    @WebMethod
    @SneakyThrows
    public void checkTaskAccess(
            @WebParam(name = "session") final SessionDTO session
    ) {
        serviceLocator.getSessionService().validate(session);
    }
}
