package ru.goloshchapov.tm.comparator;

import ru.goloshchapov.tm.api.entity.IHaveName;

import java.util.Comparator;

public final class ComparatorByName implements Comparator<IHaveName> {

    private static final ComparatorByName INSTANCE = new ComparatorByName();

    private ComparatorByName() {
    }

    public static ComparatorByName getInstance() {
        return INSTANCE;
    }

    @Override
    public int compare(final IHaveName o1, final IHaveName o2) {
        if (o1 == null || o2 == null) return 0;
        return o1.getName().compareTo(o2.getName());
    }

}
