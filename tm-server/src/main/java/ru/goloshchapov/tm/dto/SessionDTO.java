package ru.goloshchapov.tm.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Getter
@Setter
@Entity
@Table(name = "tm_session")
@NoArgsConstructor
public class SessionDTO extends AbstractEntityDTO implements Cloneable {

    @Override
    public SessionDTO clone() {
        try {
            return (SessionDTO) super.clone();
        }
        catch (CloneNotSupportedException e) {
            return null;
        }
    }

    @Getter
    @Setter
    @Column
    private Long timestamp;

    @Getter
    @Setter
    @Column(name = "user_id")
    private String userId;

    @Getter
    @Setter
    @Column
    private String signature;

}
