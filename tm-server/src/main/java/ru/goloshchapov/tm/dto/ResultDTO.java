package ru.goloshchapov.tm.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

@Getter
@Setter
@NoArgsConstructor
public final class ResultDTO extends AbstractEntityDTO {

    @Nullable
    private String result = null;

}