package ru.goloshchapov.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@Entity
@Table(name = "tm_session")
@NoArgsConstructor
public class Session extends AbstractEntity{

    @Getter
    @Setter
    @Column
    private Long timestamp;

    @Getter
    @Setter
    @OneToOne
    @JoinColumn(unique = true, name = "user_id")
    private User user;

    @Getter
    @Setter
    @Column
    private String signature;

}
