package ru.goloshchapov.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.goloshchapov.tm.enumerated.Status;

import javax.persistence.*;
import java.util.Date;

@Getter
@Setter
@MappedSuperclass
@NoArgsConstructor
public abstract class AbstractBusinessEntity extends AbstractEntity{

    @Nullable
    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;

    @Column
    @Nullable
    private String name = "";

    @Column
    @Nullable
    private String description = "";

    @NotNull
    @Enumerated(EnumType.STRING)
    private Status status = Status.NOT_STARTED;

    @Nullable
    @Column(name = "start_date")
    private Date dateStart;

    @Nullable
    @Column(name = "finish_date")
    private Date dateFinish;

    @Column
    @NotNull
    private Date created = new Date();

}
