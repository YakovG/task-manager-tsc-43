package ru.goloshchapov.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

import javax.persistence.*;

@Getter
@Setter
@Entity
@Table(name = "tm_task")
@NoArgsConstructor
public class Task extends AbstractBusinessEntity{

    @Nullable
    @ManyToOne
    @JoinColumn(name = "project_id")
    private Project project;

}
