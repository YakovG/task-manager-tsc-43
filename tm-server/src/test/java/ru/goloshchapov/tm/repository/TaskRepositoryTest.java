package ru.goloshchapov.tm.repository;

import lombok.SneakyThrows;
import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.goloshchapov.tm.api.IPropertyService;
import ru.goloshchapov.tm.api.repository.ITaskRepository;
import ru.goloshchapov.tm.api.service.*;
import ru.goloshchapov.tm.dto.ProjectDTO;
import ru.goloshchapov.tm.dto.TaskDTO;
import ru.goloshchapov.tm.marker.DataCategory;
import ru.goloshchapov.tm.dto.UserDTO;
import ru.goloshchapov.tm.service.PropertyService;
import ru.goloshchapov.tm.service.TestService;

public final class TaskRepositoryTest implements ServiceLocator {

    private UserDTO user;

    private ProjectDTO project;

    private ServiceLocator serviceLocator;

    private PropertyService propertyService;

    private TestService testService;

    @Override
    public @NotNull ITestService getTestService() {
        return testService;
    }

    @Override
    public @NotNull IConnectionService getConnectionService() {
        return null;
    }

    @Override
    public @NotNull ITaskService getTaskService() {
        return null;
    }

    @Override
    public @NotNull IProjectService getProjectService() {
        return null;
    }

    @Override
    public @NotNull IProjectTaskService getProjectTaskService() {
        return null;
    }

    @Override
    public @NotNull IUserService getUserService() {
        return null;
    }

    @Override
    public @NotNull IAuthService getAuthService() {
        return null;
    }

    @Override
    public @NotNull IPropertyService getPropertyService() {
        return propertyService;
    }

    @Override
    public @NotNull ISessionService getSessionService() {
        return null;
    }

    @Override
    public @NotNull IDataService getDataService() {
        return null;
    }

    @Before
    public void before() {
        serviceLocator = this;
        propertyService = new PropertyService();
        testService = new TestService(propertyService);
        testService.initTestTaskTable();
        user = new UserDTO();
        user.setId("ttt");
        project = new ProjectDTO();
        project.setUserId(user.getId());
    }

    @After
    @SneakyThrows
    public void after() {
        final SqlSession sqlSession = serviceLocator.getTestService().getSqlSession();
        final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
        taskRepository.clearAll();
        sqlSession.commit();
        sqlSession.close();
        testService.dropDatabase();
    }

    @Test
    @SneakyThrows
    @Category(DataCategory.class)
    public void testCreate() {
        final SqlSession sqlSession = serviceLocator.getTestService().getSqlSession();
        final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
        Assert.assertTrue(taskRepository.findAllTasks().isEmpty());
        final TaskDTO task = new TaskDTO();
        Assert.assertNotNull(task);
        Assert.assertNotNull(task.getId());
        task.setName("TASK");
        Assert.assertNotNull(task.getName());
        Assert.assertEquals("TASK", task.getName());
        taskRepository.add(task);
        sqlSession.commit();
        Assert.assertFalse(taskRepository.findAllTasks().isEmpty());
        final TaskDTO taskById = taskRepository.findTaskById(task.getId());
        Assert.assertNotNull(taskById);
        Assert.assertEquals(taskById.getId(), task.getId());
        sqlSession.close();
    }

    @Test
    @SneakyThrows
    @Category(DataCategory.class)
    public void testRemove() {
        final SqlSession sqlSession = serviceLocator.getTestService().getSqlSession();
        final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
        Assert.assertTrue(taskRepository.findAllTasks().isEmpty());
        final TaskDTO task = new TaskDTO();
        Assert.assertNotNull(task);
        Assert.assertNotNull(task.getId());
        task.setName("TASK");
        Assert.assertNotNull(task.getName());
        Assert.assertEquals("TASK", task.getName());
        taskRepository.add(task);
        sqlSession.commit();
        Assert.assertFalse(taskRepository.findAllTasks().isEmpty());
        final TaskDTO taskById = taskRepository.findTaskById(task.getId());
        Assert.assertNotNull(taskById);
        Assert.assertEquals(taskById.getId(), task.getId());
        taskRepository.removeTaskById(taskById.getId());
        Assert.assertTrue(taskRepository.findAllTasks().isEmpty());
        sqlSession.close();
    }

    @Test
    @SneakyThrows
    @Category(DataCategory.class)
    public void testClear() {
        final SqlSession sqlSession = serviceLocator.getTestService().getSqlSession();
        final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
        Assert.assertTrue(taskRepository.findAllTasks().isEmpty());
        final TaskDTO task = new TaskDTO();
        final TaskDTO task1 = new TaskDTO();
        Assert.assertNotNull(task);
        Assert.assertNotNull(task1);
        taskRepository.add(task);
        sqlSession.commit();
        taskRepository.add(task1);
        sqlSession.commit();
        Assert.assertFalse(taskRepository.findAllTasks().isEmpty());
        taskRepository.clearAll();
        sqlSession.commit();
        Assert.assertTrue(taskRepository.findAllTasks().isEmpty());
        sqlSession.close();
    }

    @Test
    @SneakyThrows
    @Category(DataCategory.class)
    public void testSize() {
        final SqlSession sqlSession = serviceLocator.getTestService().getSqlSession();
        final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
        Assert.assertTrue(taskRepository.findAllTasks().isEmpty());
        final TaskDTO task = new TaskDTO();
        final TaskDTO task1 = new TaskDTO();
        Assert.assertNotNull(task);
        Assert.assertNotNull(task1);
        taskRepository.add(task);
        taskRepository.add(task1);
        sqlSession.commit();
        Assert.assertFalse(taskRepository.findAllTasks().isEmpty());
        Assert.assertEquals(2,taskRepository.size());
        sqlSession.close();
    }

    @Test
    @SneakyThrows
    @Category(DataCategory.class)
    public void testFindAllByProjectId() {
        final SqlSession sqlSession = serviceLocator.getTestService().getSqlSession();
        final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
        Assert.assertTrue(taskRepository.findAllTasks().isEmpty());
        final TaskDTO task = new TaskDTO();
        Assert.assertNotNull(task);
        Assert.assertNotNull(task.getId());
        task.setName("TASK");
        Assert.assertNotNull(task.getName());
        Assert.assertEquals("TASK", task.getName());
        task.setProjectId("pppttt");
        Assert.assertNotNull(task.getProjectId());
        Assert.assertEquals("pppttt", task.getProjectId());
        task.setUserId(user.getId());
        taskRepository.add(task);
        sqlSession.commit();
        Assert.assertFalse(taskRepository.findAllTasks().isEmpty());
        Assert.assertFalse(taskRepository.findAllByProjectId(user.getId(), "pppttt").isEmpty());
        sqlSession.close();
    }

    @Test
    @SneakyThrows
    @Category(DataCategory.class)
    public void testRemoveAllByProjectId() {
        final SqlSession sqlSession = serviceLocator.getTestService().getSqlSession();
        final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
        Assert.assertTrue(taskRepository.findAllTasks().isEmpty());
        final TaskDTO task = new TaskDTO();
        Assert.assertNotNull(task);
        Assert.assertNotNull(task.getId());
        task.setName("TASK");
        Assert.assertNotNull(task.getName());
        Assert.assertEquals("TASK", task.getName());
        task.setProjectId("PROJECT_ID");
        Assert.assertNotNull(task.getProjectId());
        Assert.assertEquals("PROJECT_ID", task.getProjectId());
        task.setUserId(user.getId());
        taskRepository.add(task);
        sqlSession.commit();
        Assert.assertFalse(taskRepository.findAllTasks().isEmpty());
        Assert.assertFalse(taskRepository.findAllByProjectId(user.getId(), "PROJECT_ID").isEmpty());
        taskRepository.removeAllByProjectId(user.getId(), "PROJECT_ID");
        sqlSession.commit();
        Assert.assertTrue(taskRepository.findAllByProjectId(user.getId(), "PROJECT_ID").isEmpty());
        sqlSession.close();
    }

    @Test
    @SneakyThrows
    @Category(DataCategory.class)
    public void testBindUnbind() {
        final SqlSession sqlSession = serviceLocator.getTestService().getSqlSession();
        final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
        Assert.assertTrue(taskRepository.findAllTasks().isEmpty());
        final TaskDTO task = new TaskDTO();
        Assert.assertNotNull(task);
        Assert.assertNotNull(task.getId());
        task.setName("TASK");
        Assert.assertNotNull(task.getName());
        Assert.assertEquals("TASK", task.getName());
        task.setUserId(user.getId());
        Assert.assertNotNull(task.getUserId());
        Assert.assertEquals(user.getId(), task.getUserId());
        taskRepository.add(task);
        sqlSession.commit();
        Assert.assertFalse(taskRepository.findAllTasks().isEmpty());
        Assert.assertTrue(taskRepository.findAllByProjectId(user.getId(), project.getId()).isEmpty());
        taskRepository.bindToProjectById(user.getId(), task.getId(), project.getId());
        sqlSession.commit();
        Assert.assertFalse(taskRepository.findAllByProjectId(user.getId(), project.getId()).isEmpty());
        taskRepository.unbindFromProjectById(user.getId(), task.getId(), null);
        sqlSession.commit();
        Assert.assertTrue(taskRepository.findAllByProjectId(user.getId(), project.getId()).isEmpty());
        sqlSession.close();
    }
}
