package ru.goloshchapov.tm.repository;

import lombok.SneakyThrows;
import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.goloshchapov.tm.api.IPropertyService;
import ru.goloshchapov.tm.api.repository.ISessionRepository;
import ru.goloshchapov.tm.api.service.*;
import ru.goloshchapov.tm.dto.SessionDTO;
import ru.goloshchapov.tm.marker.DataCategory;
import ru.goloshchapov.tm.service.PropertyService;
import ru.goloshchapov.tm.service.TestService;

import java.util.Date;

public final class SessionRepositoryTest implements ServiceLocator{

    private ServiceLocator serviceLocator;

    private PropertyService propertyService;

    private TestService testService;

    @Override
    public @NotNull IConnectionService getConnectionService() {
        return null;
    }

    @Override
    public @NotNull ITestService getTestService() { return testService;}

    @Override
    public @NotNull ITaskService getTaskService() {
        return null;
    }

    @Override
    public @NotNull IProjectService getProjectService() {
        return null;
    }

    @Override
    public @NotNull IProjectTaskService getProjectTaskService() {
        return null;
    }

    @Override
    public @NotNull IUserService getUserService() {
        return null;
    }

    @Override
    public @NotNull IAuthService getAuthService() {
        return null;
    }

    @Override
    public @NotNull IPropertyService getPropertyService() {
        return propertyService;
    }

    @Override
    public @NotNull ISessionService getSessionService() {
        return null;
    }

    @Override
    public @NotNull IDataService getDataService() {
        return null;
    }

    @Before
    @SneakyThrows
    public void before() {
        serviceLocator = this;
        propertyService = new PropertyService();
        testService = new TestService(propertyService);
        testService.initTestSessionTable();
    }

    @After
    @SneakyThrows
    public void after() {
        final SqlSession sqlSession = serviceLocator.getTestService().getSqlSession();
        final ISessionRepository sessionRepository = sqlSession.getMapper(ISessionRepository.class);
        sessionRepository.clear();
        sqlSession.commit();
        sqlSession.close();
        testService.dropDatabase();
    }

    @Test
    @SneakyThrows
    @Category(DataCategory.class)
    public void testAdd() {
        final SqlSession sqlSession = serviceLocator.getTestService().getSqlSession();
        final ISessionRepository sessionRepository = sqlSession.getMapper(ISessionRepository.class);
        Assert.assertTrue(sessionRepository.findAllSession().isEmpty());
        final SessionDTO session = new SessionDTO();
        Assert.assertNotNull(session);
        Assert.assertNotNull(session.getId());
        session.setUserId("test");
        Assert.assertNotNull(session.getUserId());
        Assert.assertEquals("test", session.getUserId());
        @NotNull final Date date = new Date();
        session.setTimestamp(date.getTime());
        Assert.assertNotNull(session.getTimestamp());
        Assert.assertEquals(date, new Date(session.getTimestamp()));
        session.setSignature("sign");
        Assert.assertNotNull(session.getSignature());
        Assert.assertEquals("sign", session.getSignature());
        sessionRepository.add(session);
        sqlSession.commit();
        Assert.assertFalse(sessionRepository.findAllSession().isEmpty());
        final SessionDTO sessionById = sessionRepository.findSessionById(session.getId());
        Assert.assertNotNull(sessionById);
        Assert.assertEquals(sessionById.getId(), session.getId());
        sqlSession.close();
    }

    @Test
    @SneakyThrows
    @Category(DataCategory.class)
    public void testFindByUserId() {
        final SqlSession sqlSession = serviceLocator.getTestService().getSqlSession();
        final ISessionRepository sessionRepository = sqlSession.getMapper(ISessionRepository.class);
        Assert.assertTrue(sessionRepository.findAllSession().isEmpty());
        final SessionDTO session = new SessionDTO();
        Assert.assertNotNull(session);
        Assert.assertNotNull(session.getId());
        session.setUserId("test");
        Assert.assertNotNull(session.getUserId());
        Assert.assertEquals("test", session.getUserId());
        @NotNull final Date date = new Date();
        session.setTimestamp(date.getTime());
        Assert.assertNotNull(session.getTimestamp());
        Assert.assertEquals(date, new Date(session.getTimestamp()));
        session.setSignature("sign");
        Assert.assertNotNull(session.getSignature());
        Assert.assertEquals("sign", session.getSignature());
        sessionRepository.add(session);
        sqlSession.commit();
        Assert.assertFalse(sessionRepository.findAllSession().isEmpty());
        final SessionDTO sessionById = sessionRepository.findSessionById(session.getId());
        Assert.assertNotNull(sessionById);
        Assert.assertEquals(sessionById.getId(), session.getId());
        Assert.assertNotNull(sessionRepository.findByUserId("test"));
        final SessionDTO session1 = sessionRepository.findByUserId("test");
        Assert.assertNotNull(session1);
        Assert.assertEquals(sessionById.getId(), session1.getId());
        sqlSession.close();
    }

    @Test
    @SneakyThrows
    @Category(DataCategory.class)
    public void testContains() {
        final SqlSession sqlSession = serviceLocator.getTestService().getSqlSession();
        final ISessionRepository sessionRepository = sqlSession.getMapper(ISessionRepository.class);
        Assert.assertTrue(sessionRepository.findAllSession().isEmpty());
        final SessionDTO session = new SessionDTO();
        Assert.assertNotNull(session);
        session.setUserId("test");
        Assert.assertNotNull(session.getUserId());
        Assert.assertEquals("test", session.getUserId());
        @NotNull final Date date = new Date();
        session.setTimestamp(date.getTime());
        Assert.assertNotNull(session.getTimestamp());
        Assert.assertEquals(date, new Date(session.getTimestamp()));
        session.setSignature("sign");
        Assert.assertNotNull(session.getSignature());
        Assert.assertEquals("sign", session.getSignature());
        Assert.assertNotNull(session.getId());
        final String sessionId = session.getId();
        Assert.assertEquals(0, sessionRepository.contains(sessionId));
        sessionRepository.add(session);
        sqlSession.commit();
        Assert.assertFalse(sessionRepository.findAllSession().isEmpty());
        Assert.assertEquals(1, sessionRepository.contains(sessionId));
        sqlSession.close();
    }

    @Test
    @SneakyThrows
    @Category(DataCategory.class)
    public void testRemove() {
        final SqlSession sqlSession = serviceLocator.getTestService().getSqlSession();
        final ISessionRepository sessionRepository = sqlSession.getMapper(ISessionRepository.class);
        Assert.assertTrue(sessionRepository.findAllSession().isEmpty());
        final SessionDTO session = new SessionDTO();
        Assert.assertNotNull(session);
        Assert.assertNotNull(session.getId());
        session.setUserId("test");
        Assert.assertNotNull(session.getUserId());
        Assert.assertEquals("test", session.getUserId());
        @NotNull final Date date = new Date();
        session.setTimestamp(date.getTime());
        Assert.assertNotNull(session.getTimestamp());
        Assert.assertEquals(date, new Date(session.getTimestamp()));
        session.setSignature("sign");
        Assert.assertNotNull(session.getSignature());
        Assert.assertEquals("sign", session.getSignature());
        sessionRepository.add(session);
        sqlSession.commit();
        Assert.assertFalse(sessionRepository.findAllSession().isEmpty());
        final SessionDTO sessionById = sessionRepository.findSessionById(session.getId());
        Assert.assertNotNull(sessionById);
        Assert.assertEquals(sessionById.getId(), session.getId());
        sessionRepository.remove(session.getId());
        sqlSession.commit();
        Assert.assertTrue(sessionRepository.findAllSession().isEmpty());
        sqlSession.close();
    }
}
