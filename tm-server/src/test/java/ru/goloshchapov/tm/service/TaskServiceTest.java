package ru.goloshchapov.tm.service;

import lombok.SneakyThrows;
import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.goloshchapov.tm.api.IPropertyService;
import ru.goloshchapov.tm.api.repository.ITaskRepository;
import ru.goloshchapov.tm.api.repository.IUserRepository;
import ru.goloshchapov.tm.api.service.*;
import ru.goloshchapov.tm.dto.TaskDTO;
import ru.goloshchapov.tm.dto.UserDTO;
import ru.goloshchapov.tm.enumerated.Status;
import ru.goloshchapov.tm.marker.DataCategory;
import ru.goloshchapov.tm.dto.SessionDTO;

import static ru.goloshchapov.tm.util.HashUtil.salt;

public final class TaskServiceTest implements ServiceLocator{

    private SessionDTO session;

    private ServiceLocator serviceLocator;

    private UserDTO user;

    private ConnectionService connectionService;

    private UserService userService;

    private PropertyService propertyService;

    private TaskDTO task;

    private TestService testService;

    @Override
    public @NotNull ITestService getTestService() {
        return testService;
    }

    @Override
    public @NotNull ITaskService getTaskService() {
        return null;
    }

    @Override
    public @NotNull IProjectService getProjectService() {
        return null;
    }

    @Override
    public @NotNull IProjectTaskService getProjectTaskService() {
        return null;
    }

    @Override
    public @NotNull IUserService getUserService() {
        return userService;
    }

    @Override
    public @NotNull IAuthService getAuthService() {
        return null;
    }

    @Override
    public @NotNull IPropertyService getPropertyService() {
        return propertyService;
    }

    @Override
    public @NotNull ISessionService getSessionService() {
        return null;
    }

    @Override
    public @NotNull IDataService getDataService() {
        return null;
    }

    public @NotNull IConnectionService getConnectionService() {
        return connectionService;
    }

    @Before
    public void before() {
        serviceLocator = this;
        session = new SessionDTO();
        propertyService = new PropertyService();
        connectionService = new ConnectionService(propertyService);
        user = new UserDTO();
        user.setLogin("test");
        final String password = "test";
        user.setPasswordHash(salt(propertyService, password));
        userService = new UserService(serviceLocator);
        userService.add(user);
        task = new TaskDTO();
        task.setUserId(user.getId());
    }

    @After
    @SneakyThrows
    public void after() {
        final SqlSession sqlSession = serviceLocator.getConnectionService().getSqlSession();
        final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
        taskRepository.clearAll();
        sqlSession.commit();
        final IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);
        userRepository.clear();
        sqlSession.commit();
        sqlSession.close();
    }

    @Test
    @Category(DataCategory.class)
    public void testStartFinish() {
        final TaskService taskService = new TaskService(serviceLocator);
        Assert.assertNotNull(taskService);
        Assert.assertTrue(taskService.findAll().isEmpty());
        Assert.assertNotNull(task);
        Assert.assertNotNull(task.getId());
        task.setName("TASK");
        Assert.assertNotNull(task.getName());
        Assert.assertEquals("TASK", task.getName());
        task.setUserId(user.getId());
        Assert.assertEquals(user.getId(), task.getUserId());
        Assert.assertNotNull(taskService.add(task));
        Assert.assertFalse(taskService.findAll().isEmpty());
        @Nullable TaskDTO task1 = taskService.startOneById(user.getId(), task.getId());
        Assert.assertNotNull(task1);
        Assert.assertEquals(task.getId(), task1.getId());
        Assert.assertEquals(Status.IN_PROGRESS,task1.getStatus());
        task1 = taskService.finishOneById(user.getId(), task.getId());
        Assert.assertNotNull(task1);
        Assert.assertEquals(task.getId(), task1.getId());
        Assert.assertEquals(Status.COMPLETE,task1.getStatus());
    }

    @Test
    @Category(DataCategory.class)
    public void testAddUpdate() {
        final TaskService taskService = new TaskService(serviceLocator);
        Assert.assertNotNull(taskService);
        Assert.assertTrue(taskService.findAll().isEmpty());
        Assert.assertNotNull(taskService.add(user.getId(),"TASK","DESCRIPTION"));
        Assert.assertFalse(taskService.findAll().isEmpty());
        Assert.assertFalse(taskService.findAllByUserId(user.getId()).isEmpty());
        Assert.assertEquals("DESCRIPTION", taskService.findOneByName("TASK").getDescription());
        Assert.assertNotNull(taskService.findOneByName("TASK"));
        final TaskDTO project1 = taskService.findOneByName("TASK");
        taskService.updateOneById(user.getId(), project1.getId(), "NAME", "DESC");
        Assert.assertEquals("NAME", taskService.findOneById(project1.getId()).getName());
        Assert.assertEquals("DESC", taskService.findOneById(project1.getId()).getDescription());
    }

    @Test
    @Category(DataCategory.class)
    public void testChangeStatus() {
        final TaskService taskService = new TaskService(serviceLocator);
        Assert.assertNotNull(taskService);
        Assert.assertTrue(taskService.findAll().isEmpty());
        Assert.assertNotNull(task);
        Assert.assertNotNull(task.getId());
        task.setName("TASK");
        Assert.assertNotNull(task.getName());
        Assert.assertEquals("TASK", task.getName());
        task.setUserId(user.getId());
        Assert.assertEquals(user.getId(), task.getUserId());
        Assert.assertNotNull(taskService.add(task));
        Assert.assertFalse(taskService.findAll().isEmpty());
        Assert.assertNotNull(
                taskService.changeOneStatusByName(user.getId(),"TASK", "COMPLETE")
        );
        Assert.assertEquals(Status.COMPLETE, taskService.findOneByName("TASK").getStatus());
        Assert.assertNotNull(
                taskService.changeOneStatusById(user.getId(),task.getId(), "IN_PROGRESS")
        );
        Assert.assertEquals(Status.IN_PROGRESS, taskService.findOneById(task.getId()).getStatus());
    }

}
